This is my entry to the Morley Group Dec 2013 Internship Developer Challenge.

Requirements
------------
The requirements document is POS Project Requirements.docx located in the root of the project. A quick overview is: build a point-of-sale application in C# that stores things in a list, allows custom entry, and POSTs to a web service endpoint.

Features
--------
I went a bit overboard and included lots of things not in the requirements. Here is the list I submitted with my entry:

 * Input validation on all forms (and the labels turn red for bad input)
 * Properly handles XML special characters (like '<>"&) in text fields
 * Associate log-in sets Associate ID (and refuses blank ID's)
 * Editable order items
 * Deletable order items
 * Custom icon
 * Adjustable station properties (like TableNumber, StoreNumber, ...)
 * Station properties persist over different runs.
 * Auto-incrementing order number
 * Order timestamps based on actual submission times
 * Rounds tax to the nearest cent
 * Supports tax rates from 0 to 100%
 * Nice background image
 * Transparent and translucent labels
 * (Untested) Should only send the first 64KiB of large text fields.

Building
--------
The code was written under Visual Studio Ultimate 2013. To build, open the WindowsFormsApplication1.sln file and click BUILD. Though I haven't tried it, this should work under all versions of Visual Studio.

If you also have Ultimate (or Professional, I believe) you have the Microsoft Fakes library available and thus can run the test suite. The Morley Group specifically told me that unit tests were not required. However, I feel too uncomfortable submitting untested code to work without unit tests. Since I was going to write the tests anyway, I decided that including them would not hurt.

The tests are in the WindowsFormsApplication1.UnitTest project. The *.CodedUITest is empty. I originally intended to make it too, but decided the payoff was minimal over just having a manual test script (which you can find in WindowsFormsApplication1/manual_testing.txt).
