﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;

namespace WindowsFormsApplication1
{
    class Station
    {
        /// <summary>
        /// the ID of the associate operating the terminal
        /// </summary>
        /// <remarks>Will be truncated to the first 64K characters and nulls will be replaced by empty strings.</remarks>
        public string associateID
        {
            get { return _associateID; }
            set { _associateID = Utils.First64K(value ?? ""); }
        }

        public UInt64 nextOrderNumber { get; set; }
        public UInt64 storeNumber { get; set; }
        public UInt64 terminalNumber { get; set; }
        public UInt64 tableNumber { get; set; }
        
        /// <summary>
        /// The id of the cash drawer used by this station
        /// </summary>
        /// <remarks>Will be truncated to the first 64K characters and nulls will be replaced by empty strings.</remarks>
        public string cashDrawer
        {
            get { return _cashDrawer; }
            set { _cashDrawer = Utils.First64K(value ?? ""); }
        }

        /// <summary>
        /// 0..1 the amount of sales tax charged by the jurisdiction in which the station resides
        /// </summary>
        /// <remarks>Throws an ArgumentException when set to a value outside the range 0..1 inclusive.</remarks>
        public Decimal taxRate
        {
            get { return _taxRate; }
            set
            {
                if (value >= 0 && value <= 1)
                {
                    _taxRate = value;
                }
                else
                {
                    throw new ArgumentException("Tax rate must be a fraction in the range 0..1 inclusive.");
                }
            }
        }

        /// <summary>
        /// Notes about this station.
        /// </summary>
        /// <remarks>Will be truncated to the first 64K characters and nulls will be replaced by empty strings.</remarks>
        public string notes
        {
            get { return _notes; }
            set { _notes = Utils.First64K(value ?? ""); }
        }

        /// <summary>
        /// The order currently being processed by this station. Cannot be null.
        /// </summary>
        /// <remarks>Throws an ArgumentException if set to a null order.</remarks>
        public Order order
        {
            get { return _order; }
            set
            {
                if (value != null)
                {
                    _order = value;
                }
                else
                {
                    throw new ArgumentException("The order property for a Station object cannot be null");
                }
            }
        }

        /// <summary>
        /// Storage for the associateID property
        /// </summary>
        string _associateID;

        /// <summary>
        /// Storage for the cashDrawer property
        /// </summary>
        string _cashDrawer;

        /// <summary>
        /// Storage for the notes property
        /// </summary>
        string _notes;

        /// <summary>
        /// Storage for the taxRate property
        /// </summary>
        Decimal _taxRate;

        /// <summary>
        /// Storage for the order property
        /// </summary>
        Order _order;

        /// <summary>
        /// Reads the settings from the application configuration file and 
        /// creates a blank order with the given seat number and to-go status
        /// </summary>
        /// <remarks>Throws a ConfigurationErrorsException if it cannot read from the configuration. 
        /// If the configuration file is uninitialized, the Station is initialized to the values from the requirements 
        /// document XML</remarks>
        /// <param name="seatNumber">The seat number for the blank order created.</param>
        /// <param name="isToGoOrder">The to-go status of the blank order created.</param>
        public Station(UInt32 seatNumber, Boolean isToGoOrder)
        {
            this.associateID = Properties.Settings.Default.associateID;
            this.cashDrawer = Properties.Settings.Default.cashDrawer;
            this.nextOrderNumber = Properties.Settings.Default.nextOrderNumber;
            this.notes = Properties.Settings.Default.notes;
            this.order = new Order(seatNumber, isToGoOrder);
            this.storeNumber = Properties.Settings.Default.storeNumber;
            this.tableNumber = Properties.Settings.Default.tableNumber;
            this.taxRate = Properties.Settings.Default.taxRate;
            this.terminalNumber = Properties.Settings.Default.terminalNumber;
        }

        /// <summary>
        /// Assigns the parameters to their identically named properties
        /// </summary>
        /// <param name="associateID">the value for the associateID property</param>
        /// <param name="nextOrderNumber">the value for the nextOrderNumber property</param>
        /// <param name="storeNumber">the value for the storeNumber property</param>
        /// <param name="terminalNumber">the value for the terminalNumber property</param>
        /// <param name="tableNumber">the value for the tableNumber property</param>
        /// <param name="cashDrawer">the value for the cashDrawer property</param>
        /// <param name="taxRate">the value for the taxRate property</param>
        /// <param name="notes">the value for the notes property</param>
        /// <param name="order">the value for the order property</param>
        internal Station(string associateID, UInt64 nextOrderNumber, UInt64 storeNumber,
            UInt64 terminalNumber, UInt64 tableNumber, 
            string cashDrawer, Decimal taxRate, string notes,
            Order order)
        {
            this.associateID = associateID;
            this.nextOrderNumber = nextOrderNumber;
            this.storeNumber = storeNumber;
            this.terminalNumber = terminalNumber;
            this.tableNumber = tableNumber;
            this.cashDrawer = cashDrawer;
            this.taxRate = taxRate;
            this.notes = notes;
            this.order = order;
        }

        /// <summary>
        /// Returns the xml that will be posted to the server to represent
        /// the current order being processed at this station.
        /// </summary>
        /// <remarks>Depends on Order to return a correct XML representation 
        /// of itself, but otherwise ensures that all of its own strings 
        /// are properly escaped.</remarks>
        /// <returns>XML representing the current order</returns>
        public string OrderXML()
        {
            string timestamp = Utils.NowTimestamp();
            return 
                "<Order>" +
                    "<AssociateId>"+Utils.XmlEscape(associateID)+"</AssociateId>" +
                    "<Subtotal>"+order.Subtotal()+"</Subtotal>" +
                    "<Tax>"+order.Tax(taxRate)+"</Tax>" +
                    "<Total>"+order.Total(taxRate)+"</Total>" +
                    "<OrderNumber>"+nextOrderNumber+"</OrderNumber>" +
                    "<StoreNumber>"+storeNumber+"</StoreNumber>" +
                    "<TerminalNumber>"+terminalNumber+"</TerminalNumber>" +
                    "<TableNumber>"+tableNumber+"</TableNumber><Printer/>" +
                    "<CashDrawer>"+Utils.XmlEscape(cashDrawer)+"</CashDrawer>" +
                    "<TaxRate>"+taxRate+"</TaxRate><CustomerName/>" +
                    "<Notes>"+Utils.XmlEscape(notes)+"</Notes>" +
                    "<Timestamp>" + timestamp + "</Timestamp>" +
                    "<ModifiedTimestamp>" + timestamp + "</ModifiedTimestamp>" +
                    "<OrderItems>" +order.OrderItemsXML() + "</OrderItems>" +
                "</Order>";
        }

        /// <summary>
        /// Uses the WebHelper class to post the order to the web service. 
        /// </summary>
        /// <remarks>
        /// If the post is not successful, the Station is left unchanged 
        /// and an IOException is thrown with a user-consumable error 
        /// message giving the reason for the failure. 
        /// 
        /// If the post is successful, increments the orderNumber property 
        /// and clears the current order.
        /// </remarks>
        /// <param name="helper">The WebHelper to use for the posting.</param>
        public void PostOrder(TMG.Web.WebHelper helper)
        {
            string xml = OrderXML();
            var resp = (HttpWebResponse)helper.SendPostRequest("order", Encoding.UTF8.GetBytes(xml));
            if (resp != null && resp.StatusCode == HttpStatusCode.OK)
            {
                order.items = new List<OrderItem>();
                nextOrderNumber += 1;
            }
            else
            {
                string msg = "There was an error posting the order to the web site. Most likely you can try again in a few minutes.";
                throw new System.IO.IOException(msg);
            }
        }

        /// <summary>
        /// Writes this Station's values (except the order) to the configuration file loaded in the default constructor.
        /// </summary>
        /// <remarks>Throws an ConfigurationErrorsException if it cannot write.</remarks>
        public void SaveConfig()
        {
            Properties.Settings.Default.associateID = associateID;
            Properties.Settings.Default.nextOrderNumber = nextOrderNumber;
            Properties.Settings.Default.storeNumber = storeNumber;
            Properties.Settings.Default.terminalNumber = terminalNumber;
            Properties.Settings.Default.tableNumber = tableNumber;
            Properties.Settings.Default.cashDrawer = cashDrawer;
            Properties.Settings.Default.taxRate = taxRate;
            Properties.Settings.Default.notes = notes;

            Properties.Settings.Default.Save();
        }
    }
}
