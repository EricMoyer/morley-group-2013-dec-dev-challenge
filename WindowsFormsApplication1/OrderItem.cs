﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class OrderItem
    {
        /// <summary>
        /// Description of the item to be ordered. 
        /// 
        /// May contain any character; must be escaped before xml output. Only the first 64K characters are accepted. Null values become the empty string.
        /// </summary>
        public string description
        {
            get { return _description; }
            set { _description = Utils.First64K(value ?? ""); }
        }
        /// <summary>
        /// Cost of the item in pennies
        /// </summary>
        public UInt32 price { get; set; }
        /// <summary>
        /// User notes for this item
        /// 
        /// May contain any character; must be escaped before xml output. Only the first 64K characters are accepted.  Null values become the empty string.
        /// </summary>
        public string notes
        {
            get { return _notes; }
            set { _notes = Utils.First64K(value ?? ""); }
        }


        /// <summary>
        /// Storage for the description property
        /// </summary>
        private string _description;

        /// <summary>
        /// Storage for the notes property
        /// </summary>
        private string _notes;


        /// <summary>
        /// Create an initialized OrderItem
        /// </summary>
        /// <param name="description">Description of the item to be ordered.</param>
        /// <param name="price">Cost of the item in pennies</param>
        /// <param name="notes">User notes for this item</param>
        public OrderItem(string description, UInt32 price, string notes)
        {
            this.description = description;
            this.price = price;
            this.notes = notes;
        }

        /// <summary>
        /// Returns an <OrderItem> subtree representing this item. Escapes xml characters in strings
        ///
        /// For example, an OrderItem o with a description="Thunder Burger", price = 365, notes="<No anchovies>", if you called o.xml(3,true,5) would return:
        ///
        /// &lt;OrderItem&gt;&lt;SequenceNumber&gt;5&lt;/SequenceNumber&gt;&lt;Description&gt;Thunder Burger&lt;/Description&gt;&lt;Price&gt;365&lt;/Price&gt;&lt;Notes&gt;&amp;lt;No anchovies&amp;gt;&lt;/Notes&gt;&lt;SeatNumber&gt;3&lt;/SeatNumber&gt;&lt;Quantity&gt;1&lt;/Quantity&gt;&lt;Togo&gt;0&lt;/Togo&gt;&lt;/OrderItem&gt;
        /// </summary>
        /// <param name="seatNumber">the seat number of the customer making the order of which this item is a part</param>
        /// <param name="isToGoOrder">true if the order is being taken to-go, false otherwise</param>
        /// <param name="sequenceNumber">the sequence number for this item</param>
        /// <returns>The xml subtree representing this item.</returns>
        public string Xml(UInt32 seatNumber, bool isToGoOrder, UInt32 sequenceNumber)
        {
            var s = new StringBuilder();
            s.Append("<OrderItem>");
            s.Append("<SequenceNumber>" + sequenceNumber + "</SequenceNumber>");
            s.Append("<Description>" + Utils.XmlEscape(description) + "</Description>");
            s.Append("<Price>" + price + "</Price>");
            s.Append("<Notes>" + Utils.XmlEscape(notes) + "</Notes>");
            s.Append("<SeatNumber>" + seatNumber + "</SeatNumber>");
            s.Append("<Quantity>1</Quantity>");
            s.Append("<Togo>" + (isToGoOrder ? 1 : 0) + "</Togo>");
            s.Append("</OrderItem>");
            return s.ToString();
        }
    }
}
