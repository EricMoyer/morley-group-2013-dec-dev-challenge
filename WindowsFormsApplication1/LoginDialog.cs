﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class LoginDialog : Form
    {
        public string associateID { 
            get { return associateIDTextBox.Text; }
            set { associateIDTextBox.Text = value; }
        }

        public LoginDialog()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (associateIDTextBox.Text.Trim().Length == 0)
            {
                DialogResult = DialogResult.None;
            }
        }
    }
}
