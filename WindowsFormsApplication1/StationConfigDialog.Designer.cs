﻿namespace WindowsFormsApplication1
{
    partial class StationConfigDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StationConfigDialog));
            this.nextOrderNumberLabel = new System.Windows.Forms.Label();
            this.nextOrderNumberTextBox = new System.Windows.Forms.TextBox();
            this.storeNumberTextBox = new System.Windows.Forms.TextBox();
            this.storeNumberLabel = new System.Windows.Forms.Label();
            this.terminalNumberTextBox = new System.Windows.Forms.TextBox();
            this.terminalNumberLabel = new System.Windows.Forms.Label();
            this.tableNumberTextBox = new System.Windows.Forms.TextBox();
            this.tableNumberLabel = new System.Windows.Forms.Label();
            this.taxRateTextBox = new System.Windows.Forms.TextBox();
            this.taxRateLabel = new System.Windows.Forms.Label();
            this.cashDrawerTextBox = new System.Windows.Forms.TextBox();
            this.cashDrawerLabel = new System.Windows.Forms.Label();
            this.notesTextBox = new System.Windows.Forms.TextBox();
            this.notesLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // nextOrderNumberLabel
            // 
            this.nextOrderNumberLabel.AutoSize = true;
            this.nextOrderNumberLabel.Location = new System.Drawing.Point(12, 9);
            this.nextOrderNumberLabel.Name = "nextOrderNumberLabel";
            this.nextOrderNumberLabel.Size = new System.Drawing.Size(73, 13);
            this.nextOrderNumberLabel.TabIndex = 0;
            this.nextOrderNumberLabel.Text = "Order Number";
            // 
            // nextOrderNumberTextBox
            // 
            this.nextOrderNumberTextBox.Location = new System.Drawing.Point(12, 26);
            this.nextOrderNumberTextBox.Name = "nextOrderNumberTextBox";
            this.nextOrderNumberTextBox.Size = new System.Drawing.Size(259, 20);
            this.nextOrderNumberTextBox.TabIndex = 1;
            // 
            // storeNumberTextBox
            // 
            this.storeNumberTextBox.Location = new System.Drawing.Point(12, 65);
            this.storeNumberTextBox.Name = "storeNumberTextBox";
            this.storeNumberTextBox.Size = new System.Drawing.Size(259, 20);
            this.storeNumberTextBox.TabIndex = 3;
            // 
            // storeNumberLabel
            // 
            this.storeNumberLabel.AutoSize = true;
            this.storeNumberLabel.Location = new System.Drawing.Point(9, 49);
            this.storeNumberLabel.Name = "storeNumberLabel";
            this.storeNumberLabel.Size = new System.Drawing.Size(72, 13);
            this.storeNumberLabel.TabIndex = 2;
            this.storeNumberLabel.Text = "Store Number";
            // 
            // terminalNumberTextBox
            // 
            this.terminalNumberTextBox.Location = new System.Drawing.Point(12, 104);
            this.terminalNumberTextBox.Name = "terminalNumberTextBox";
            this.terminalNumberTextBox.Size = new System.Drawing.Size(259, 20);
            this.terminalNumberTextBox.TabIndex = 5;
            // 
            // terminalNumberLabel
            // 
            this.terminalNumberLabel.AutoSize = true;
            this.terminalNumberLabel.Location = new System.Drawing.Point(9, 88);
            this.terminalNumberLabel.Name = "terminalNumberLabel";
            this.terminalNumberLabel.Size = new System.Drawing.Size(87, 13);
            this.terminalNumberLabel.TabIndex = 4;
            this.terminalNumberLabel.Text = "Terminal Number";
            // 
            // tableNumberTextBox
            // 
            this.tableNumberTextBox.Location = new System.Drawing.Point(12, 143);
            this.tableNumberTextBox.Name = "tableNumberTextBox";
            this.tableNumberTextBox.Size = new System.Drawing.Size(259, 20);
            this.tableNumberTextBox.TabIndex = 7;
            // 
            // tableNumberLabel
            // 
            this.tableNumberLabel.AutoSize = true;
            this.tableNumberLabel.Location = new System.Drawing.Point(9, 127);
            this.tableNumberLabel.Name = "tableNumberLabel";
            this.tableNumberLabel.Size = new System.Drawing.Size(74, 13);
            this.tableNumberLabel.TabIndex = 6;
            this.tableNumberLabel.Text = "Table Number";
            // 
            // taxRateTextBox
            // 
            this.taxRateTextBox.Location = new System.Drawing.Point(12, 182);
            this.taxRateTextBox.Name = "taxRateTextBox";
            this.taxRateTextBox.Size = new System.Drawing.Size(259, 20);
            this.taxRateTextBox.TabIndex = 9;
            // 
            // taxRateLabel
            // 
            this.taxRateLabel.AutoSize = true;
            this.taxRateLabel.Location = new System.Drawing.Point(8, 166);
            this.taxRateLabel.Name = "taxRateLabel";
            this.taxRateLabel.Size = new System.Drawing.Size(96, 13);
            this.taxRateLabel.TabIndex = 8;
            this.taxRateLabel.Text = "Tax Rate (percent)";
            // 
            // cashDrawerTextBox
            // 
            this.cashDrawerTextBox.Location = new System.Drawing.Point(12, 221);
            this.cashDrawerTextBox.Name = "cashDrawerTextBox";
            this.cashDrawerTextBox.Size = new System.Drawing.Size(259, 20);
            this.cashDrawerTextBox.TabIndex = 11;
            // 
            // cashDrawerLabel
            // 
            this.cashDrawerLabel.AutoSize = true;
            this.cashDrawerLabel.Location = new System.Drawing.Point(9, 205);
            this.cashDrawerLabel.Name = "cashDrawerLabel";
            this.cashDrawerLabel.Size = new System.Drawing.Size(68, 13);
            this.cashDrawerLabel.TabIndex = 10;
            this.cashDrawerLabel.Text = "Cash Drawer";
            // 
            // notesTextBox
            // 
            this.notesTextBox.Location = new System.Drawing.Point(12, 260);
            this.notesTextBox.Multiline = true;
            this.notesTextBox.Name = "notesTextBox";
            this.notesTextBox.Size = new System.Drawing.Size(259, 260);
            this.notesTextBox.TabIndex = 13;
            // 
            // notesLabel
            // 
            this.notesLabel.AutoSize = true;
            this.notesLabel.Location = new System.Drawing.Point(9, 244);
            this.notesLabel.Name = "notesLabel";
            this.notesLabel.Size = new System.Drawing.Size(35, 13);
            this.notesLabel.TabIndex = 12;
            this.notesLabel.Text = "Notes";
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(11, 526);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 14;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(195, 526);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 15;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // StationConfigDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 561);
            this.ControlBox = false;
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.notesTextBox);
            this.Controls.Add(this.notesLabel);
            this.Controls.Add(this.cashDrawerTextBox);
            this.Controls.Add(this.cashDrawerLabel);
            this.Controls.Add(this.taxRateTextBox);
            this.Controls.Add(this.taxRateLabel);
            this.Controls.Add(this.tableNumberTextBox);
            this.Controls.Add(this.tableNumberLabel);
            this.Controls.Add(this.terminalNumberTextBox);
            this.Controls.Add(this.terminalNumberLabel);
            this.Controls.Add(this.storeNumberTextBox);
            this.Controls.Add(this.storeNumberLabel);
            this.Controls.Add(this.nextOrderNumberTextBox);
            this.Controls.Add(this.nextOrderNumberLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StationConfigDialog";
            this.Text = "Configure Station";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nextOrderNumberLabel;
        private System.Windows.Forms.TextBox nextOrderNumberTextBox;
        private System.Windows.Forms.TextBox storeNumberTextBox;
        private System.Windows.Forms.Label storeNumberLabel;
        private System.Windows.Forms.TextBox terminalNumberTextBox;
        private System.Windows.Forms.Label terminalNumberLabel;
        private System.Windows.Forms.TextBox tableNumberTextBox;
        private System.Windows.Forms.Label tableNumberLabel;
        private System.Windows.Forms.TextBox taxRateTextBox;
        private System.Windows.Forms.Label taxRateLabel;
        private System.Windows.Forms.TextBox cashDrawerTextBox;
        private System.Windows.Forms.Label cashDrawerLabel;
        private System.Windows.Forms.TextBox notesTextBox;
        private System.Windows.Forms.Label notesLabel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
    }
}