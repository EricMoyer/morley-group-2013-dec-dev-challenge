﻿namespace WindowsFormsApplication1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.skyBurgerLabel = new System.Windows.Forms.Label();
            this.seatNumberLabel = new System.Windows.Forms.Label();
            this.toGoCheckBox = new System.Windows.Forms.CheckBox();
            this.hamburgerButton = new System.Windows.Forms.Button();
            this.thunderBurgerButton = new System.Windows.Forms.Button();
            this.thunderBurgerWithCheeseButton = new System.Windows.Forms.Button();
            this.planetFriesButton = new System.Windows.Forms.Button();
            this.asteroidFriesButton = new System.Windows.Forms.Button();
            this.meteorFriesButton = new System.Windows.Forms.Button();
            this.sodaHurricaneButton = new System.Windows.Forms.Button();
            this.sodaStormButton = new System.Windows.Forms.Button();
            this.sodaDrizzleButton = new System.Windows.Forms.Button();
            this.specialOrderButton = new System.Windows.Forms.Button();
            this.skyBlizzardButton = new System.Windows.Forms.Button();
            this.skyFlurryButton = new System.Windows.Forms.Button();
            this.orderItemsListView = new System.Windows.Forms.ListView();
            this.descriptionColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.priceColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.notesColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.configButton = new System.Windows.Forms.Button();
            this.logoutButton = new System.Windows.Forms.Button();
            this.subtotalLabel = new System.Windows.Forms.Label();
            this.taxLabel = new System.Windows.Forms.Label();
            this.orderTotalLabel = new System.Windows.Forms.Label();
            this.editButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.submitOrderButton = new System.Windows.Forms.Button();
            this.seatNumberTextBox = new System.Windows.Forms.MaskedTextBox();
            this.skyBackgroundPictureBox = new System.Windows.Forms.PictureBox();
            this.backgroundAttributionLinkLabel = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.skyBackgroundPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // skyBurgerLabel
            // 
            this.skyBurgerLabel.AutoSize = true;
            this.skyBurgerLabel.BackColor = System.Drawing.Color.Transparent;
            this.skyBurgerLabel.Font = new System.Drawing.Font("Buxton Sketch", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.skyBurgerLabel.Location = new System.Drawing.Point(23, 21);
            this.skyBurgerLabel.Name = "skyBurgerLabel";
            this.skyBurgerLabel.Size = new System.Drawing.Size(158, 39);
            this.skyBurgerLabel.TabIndex = 0;
            this.skyBurgerLabel.Text = "Sky Burger";
            this.skyBurgerLabel.Click += new System.EventHandler(this.skyBurgerLabel_Click);
            // 
            // seatNumberLabel
            // 
            this.seatNumberLabel.AutoSize = true;
            this.seatNumberLabel.Location = new System.Drawing.Point(50, 97);
            this.seatNumberLabel.Name = "seatNumberLabel";
            this.seatNumberLabel.Size = new System.Drawing.Size(69, 13);
            this.seatNumberLabel.TabIndex = 2;
            this.seatNumberLabel.Text = "Seat Number";
            // 
            // toGoCheckBox
            // 
            this.toGoCheckBox.AutoSize = true;
            this.toGoCheckBox.Location = new System.Drawing.Point(253, 96);
            this.toGoCheckBox.Name = "toGoCheckBox";
            this.toGoCheckBox.Size = new System.Drawing.Size(56, 17);
            this.toGoCheckBox.TabIndex = 3;
            this.toGoCheckBox.Text = "To Go";
            this.toGoCheckBox.UseVisualStyleBackColor = true;
            this.toGoCheckBox.CheckedChanged += new System.EventHandler(this.toGoCheckBox_CheckedChanged);
            // 
            // hamburgerButton
            // 
            this.hamburgerButton.Location = new System.Drawing.Point(30, 119);
            this.hamburgerButton.Name = "hamburgerButton";
            this.hamburgerButton.Size = new System.Drawing.Size(89, 36);
            this.hamburgerButton.TabIndex = 4;
            this.hamburgerButton.Text = "Hamburger";
            this.hamburgerButton.UseVisualStyleBackColor = true;
            this.hamburgerButton.Click += new System.EventHandler(this.hamburgerButton_Click);
            // 
            // thunderBurgerButton
            // 
            this.thunderBurgerButton.Location = new System.Drawing.Point(125, 119);
            this.thunderBurgerButton.Name = "thunderBurgerButton";
            this.thunderBurgerButton.Size = new System.Drawing.Size(89, 36);
            this.thunderBurgerButton.TabIndex = 5;
            this.thunderBurgerButton.Text = "Thunder Burger";
            this.thunderBurgerButton.UseVisualStyleBackColor = true;
            this.thunderBurgerButton.Click += new System.EventHandler(this.thunderBurgerButton_Click);
            // 
            // thunderBurgerWithCheeseButton
            // 
            this.thunderBurgerWithCheeseButton.Location = new System.Drawing.Point(220, 119);
            this.thunderBurgerWithCheeseButton.Name = "thunderBurgerWithCheeseButton";
            this.thunderBurgerWithCheeseButton.Size = new System.Drawing.Size(89, 36);
            this.thunderBurgerWithCheeseButton.TabIndex = 6;
            this.thunderBurgerWithCheeseButton.Text = "Thunder Burger w/Cheese";
            this.thunderBurgerWithCheeseButton.UseVisualStyleBackColor = true;
            this.thunderBurgerWithCheeseButton.Click += new System.EventHandler(this.thunderBurgerWithCheeseButton_Click);
            // 
            // planetFriesButton
            // 
            this.planetFriesButton.Location = new System.Drawing.Point(220, 161);
            this.planetFriesButton.Name = "planetFriesButton";
            this.planetFriesButton.Size = new System.Drawing.Size(89, 36);
            this.planetFriesButton.TabIndex = 9;
            this.planetFriesButton.Text = "Planet Fries";
            this.planetFriesButton.UseVisualStyleBackColor = true;
            this.planetFriesButton.Click += new System.EventHandler(this.planetFriesButton_Click);
            // 
            // asteroidFriesButton
            // 
            this.asteroidFriesButton.Location = new System.Drawing.Point(125, 161);
            this.asteroidFriesButton.Name = "asteroidFriesButton";
            this.asteroidFriesButton.Size = new System.Drawing.Size(89, 36);
            this.asteroidFriesButton.TabIndex = 8;
            this.asteroidFriesButton.Text = "Asteroid Fries";
            this.asteroidFriesButton.UseVisualStyleBackColor = true;
            this.asteroidFriesButton.Click += new System.EventHandler(this.asteroidFriesButton_Click);
            // 
            // meteorFriesButton
            // 
            this.meteorFriesButton.Location = new System.Drawing.Point(30, 161);
            this.meteorFriesButton.Name = "meteorFriesButton";
            this.meteorFriesButton.Size = new System.Drawing.Size(89, 36);
            this.meteorFriesButton.TabIndex = 7;
            this.meteorFriesButton.Text = "Meteor Fries";
            this.meteorFriesButton.UseVisualStyleBackColor = true;
            this.meteorFriesButton.Click += new System.EventHandler(this.meteorFriesButton_Click);
            // 
            // sodaHurricaneButton
            // 
            this.sodaHurricaneButton.Location = new System.Drawing.Point(220, 203);
            this.sodaHurricaneButton.Name = "sodaHurricaneButton";
            this.sodaHurricaneButton.Size = new System.Drawing.Size(89, 36);
            this.sodaHurricaneButton.TabIndex = 12;
            this.sodaHurricaneButton.Text = "Soda Hurricane";
            this.sodaHurricaneButton.UseVisualStyleBackColor = true;
            this.sodaHurricaneButton.Click += new System.EventHandler(this.sodaHurricaneButton_Click);
            // 
            // sodaStormButton
            // 
            this.sodaStormButton.Location = new System.Drawing.Point(125, 203);
            this.sodaStormButton.Name = "sodaStormButton";
            this.sodaStormButton.Size = new System.Drawing.Size(89, 36);
            this.sodaStormButton.TabIndex = 11;
            this.sodaStormButton.Text = "Soda Storm";
            this.sodaStormButton.UseVisualStyleBackColor = true;
            this.sodaStormButton.Click += new System.EventHandler(this.sodaStormButton_Click);
            // 
            // sodaDrizzleButton
            // 
            this.sodaDrizzleButton.Location = new System.Drawing.Point(30, 203);
            this.sodaDrizzleButton.Name = "sodaDrizzleButton";
            this.sodaDrizzleButton.Size = new System.Drawing.Size(89, 36);
            this.sodaDrizzleButton.TabIndex = 10;
            this.sodaDrizzleButton.Text = "Soda Drizzle";
            this.sodaDrizzleButton.UseVisualStyleBackColor = true;
            this.sodaDrizzleButton.Click += new System.EventHandler(this.sodaDrizzleButton_Click);
            // 
            // specialOrderButton
            // 
            this.specialOrderButton.Location = new System.Drawing.Point(30, 287);
            this.specialOrderButton.Name = "specialOrderButton";
            this.specialOrderButton.Size = new System.Drawing.Size(89, 36);
            this.specialOrderButton.TabIndex = 15;
            this.specialOrderButton.Text = "Special Order";
            this.specialOrderButton.UseVisualStyleBackColor = true;
            this.specialOrderButton.Click += new System.EventHandler(this.specialOrderButton_Click);
            // 
            // skyBlizzardButton
            // 
            this.skyBlizzardButton.Location = new System.Drawing.Point(125, 245);
            this.skyBlizzardButton.Name = "skyBlizzardButton";
            this.skyBlizzardButton.Size = new System.Drawing.Size(89, 36);
            this.skyBlizzardButton.TabIndex = 14;
            this.skyBlizzardButton.Text = "Sky Blizzard";
            this.skyBlizzardButton.UseVisualStyleBackColor = true;
            this.skyBlizzardButton.Click += new System.EventHandler(this.skyBlizzardButton_Click);
            // 
            // skyFlurryButton
            // 
            this.skyFlurryButton.Location = new System.Drawing.Point(30, 245);
            this.skyFlurryButton.Name = "skyFlurryButton";
            this.skyFlurryButton.Size = new System.Drawing.Size(89, 36);
            this.skyFlurryButton.TabIndex = 13;
            this.skyFlurryButton.Text = "Sky Flurry";
            this.skyFlurryButton.UseVisualStyleBackColor = true;
            this.skyFlurryButton.Click += new System.EventHandler(this.skyFlurryButton_Click);
            // 
            // orderItemsListView
            // 
            this.orderItemsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.descriptionColumn,
            this.priceColumn,
            this.notesColumn});
            this.orderItemsListView.FullRowSelect = true;
            this.orderItemsListView.Location = new System.Drawing.Point(325, 119);
            this.orderItemsListView.MultiSelect = false;
            this.orderItemsListView.Name = "orderItemsListView";
            this.orderItemsListView.Size = new System.Drawing.Size(671, 517);
            this.orderItemsListView.TabIndex = 16;
            this.orderItemsListView.UseCompatibleStateImageBehavior = false;
            this.orderItemsListView.View = System.Windows.Forms.View.Details;
            // 
            // descriptionColumn
            // 
            this.descriptionColumn.Text = "Description";
            this.descriptionColumn.Width = 92;
            // 
            // priceColumn
            // 
            this.priceColumn.Text = "Price";
            this.priceColumn.Width = 66;
            // 
            // notesColumn
            // 
            this.notesColumn.Text = "Notes";
            this.notesColumn.Width = 509;
            // 
            // configButton
            // 
            this.configButton.Location = new System.Drawing.Point(840, 37);
            this.configButton.Name = "configButton";
            this.configButton.Size = new System.Drawing.Size(75, 23);
            this.configButton.TabIndex = 0;
            this.configButton.Text = "Configure";
            this.configButton.UseVisualStyleBackColor = true;
            this.configButton.Click += new System.EventHandler(this.configButton_Click);
            // 
            // logoutButton
            // 
            this.logoutButton.Location = new System.Drawing.Point(921, 37);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(75, 23);
            this.logoutButton.TabIndex = 1;
            this.logoutButton.Text = "Log out";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // subtotalLabel
            // 
            this.subtotalLabel.Location = new System.Drawing.Point(325, 97);
            this.subtotalLabel.Name = "subtotalLabel";
            this.subtotalLabel.Size = new System.Drawing.Size(197, 19);
            this.subtotalLabel.TabIndex = 19;
            this.subtotalLabel.Text = "Subtotal: 0.00";
            // 
            // taxLabel
            // 
            this.taxLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.taxLabel.Location = new System.Drawing.Point(519, 97);
            this.taxLabel.Name = "taxLabel";
            this.taxLabel.Size = new System.Drawing.Size(283, 19);
            this.taxLabel.TabIndex = 20;
            this.taxLabel.Text = "Tax: 0.00";
            this.taxLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // orderTotalLabel
            // 
            this.orderTotalLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.orderTotalLabel.Location = new System.Drawing.Point(809, 97);
            this.orderTotalLabel.Name = "orderTotalLabel";
            this.orderTotalLabel.Size = new System.Drawing.Size(187, 19);
            this.orderTotalLabel.TabIndex = 21;
            this.orderTotalLabel.Text = "Order Total: 0.00";
            this.orderTotalLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(325, 642);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(75, 23);
            this.editButton.TabIndex = 17;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(406, 642);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 23);
            this.deleteButton.TabIndex = 18;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // submitOrderButton
            // 
            this.submitOrderButton.Location = new System.Drawing.Point(912, 642);
            this.submitOrderButton.Name = "submitOrderButton";
            this.submitOrderButton.Size = new System.Drawing.Size(84, 23);
            this.submitOrderButton.TabIndex = 19;
            this.submitOrderButton.Text = "Submit Order";
            this.submitOrderButton.UseVisualStyleBackColor = true;
            this.submitOrderButton.Click += new System.EventHandler(this.submitOrderButton_Click);
            // 
            // seatNumberTextBox
            // 
            this.seatNumberTextBox.HidePromptOnLeave = true;
            this.seatNumberTextBox.Location = new System.Drawing.Point(125, 94);
            this.seatNumberTextBox.Mask = "099999999";
            this.seatNumberTextBox.Name = "seatNumberTextBox";
            this.seatNumberTextBox.Size = new System.Drawing.Size(89, 20);
            this.seatNumberTextBox.TabIndex = 2;
            this.seatNumberTextBox.Text = "1";
            this.seatNumberTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.seatNumberTextBox.TextChanged += new System.EventHandler(this.seatNumberTextBox_TextChanged);
            // 
            // skyBackgroundPictureBox
            // 
            this.skyBackgroundPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("skyBackgroundPictureBox.BackgroundImage")));
            this.skyBackgroundPictureBox.Location = new System.Drawing.Point(0, 0);
            this.skyBackgroundPictureBox.Name = "skyBackgroundPictureBox";
            this.skyBackgroundPictureBox.Size = new System.Drawing.Size(1024, 768);
            this.skyBackgroundPictureBox.TabIndex = 22;
            this.skyBackgroundPictureBox.TabStop = false;
            // 
            // backgroundAttributionLinkLabel
            // 
            this.backgroundAttributionLinkLabel.AutoSize = true;
            this.backgroundAttributionLinkLabel.Location = new System.Drawing.Point(664, 707);
            this.backgroundAttributionLinkLabel.Name = "backgroundAttributionLinkLabel";
            this.backgroundAttributionLinkLabel.Size = new System.Drawing.Size(332, 13);
            this.backgroundAttributionLinkLabel.TabIndex = 23;
            this.backgroundAttributionLinkLabel.TabStop = true;
            this.backgroundAttributionLinkLabel.Text = "Background used under a CC-BY 2.0 license from Flickr user SoraZG";
            this.backgroundAttributionLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.backgroundAttributionLinkLabel_LinkClicked);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.backgroundAttributionLinkLabel);
            this.Controls.Add(this.seatNumberTextBox);
            this.Controls.Add(this.submitOrderButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.orderTotalLabel);
            this.Controls.Add(this.subtotalLabel);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.configButton);
            this.Controls.Add(this.orderItemsListView);
            this.Controls.Add(this.specialOrderButton);
            this.Controls.Add(this.skyBlizzardButton);
            this.Controls.Add(this.skyFlurryButton);
            this.Controls.Add(this.sodaHurricaneButton);
            this.Controls.Add(this.sodaStormButton);
            this.Controls.Add(this.sodaDrizzleButton);
            this.Controls.Add(this.planetFriesButton);
            this.Controls.Add(this.asteroidFriesButton);
            this.Controls.Add(this.meteorFriesButton);
            this.Controls.Add(this.thunderBurgerWithCheeseButton);
            this.Controls.Add(this.thunderBurgerButton);
            this.Controls.Add(this.hamburgerButton);
            this.Controls.Add(this.toGoCheckBox);
            this.Controls.Add(this.seatNumberLabel);
            this.Controls.Add(this.skyBurgerLabel);
            this.Controls.Add(this.taxLabel);
            this.Controls.Add(this.skyBackgroundPictureBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Sky Burger";
            ((System.ComponentModel.ISupportInitialize)(this.skyBackgroundPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label skyBurgerLabel;
        private System.Windows.Forms.Label seatNumberLabel;
        private System.Windows.Forms.CheckBox toGoCheckBox;
        private System.Windows.Forms.Button hamburgerButton;
        private System.Windows.Forms.Button thunderBurgerButton;
        private System.Windows.Forms.Button thunderBurgerWithCheeseButton;
        private System.Windows.Forms.Button planetFriesButton;
        private System.Windows.Forms.Button asteroidFriesButton;
        private System.Windows.Forms.Button meteorFriesButton;
        private System.Windows.Forms.Button sodaHurricaneButton;
        private System.Windows.Forms.Button sodaStormButton;
        private System.Windows.Forms.Button sodaDrizzleButton;
        private System.Windows.Forms.Button specialOrderButton;
        private System.Windows.Forms.Button skyBlizzardButton;
        private System.Windows.Forms.Button skyFlurryButton;
        private System.Windows.Forms.ListView orderItemsListView;
        private System.Windows.Forms.Button configButton;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.Label subtotalLabel;
        private System.Windows.Forms.Label taxLabel;
        private System.Windows.Forms.Label orderTotalLabel;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button submitOrderButton;
        private System.Windows.Forms.MaskedTextBox seatNumberTextBox;
        private System.Windows.Forms.ColumnHeader descriptionColumn;
        private System.Windows.Forms.ColumnHeader priceColumn;
        private System.Windows.Forms.ColumnHeader notesColumn;
        private System.Windows.Forms.PictureBox skyBackgroundPictureBox;
        private System.Windows.Forms.LinkLabel backgroundAttributionLinkLabel;
    }
}

