﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WindowsFormsApplication1
{
    class Utils
    {
        public static string HexDump(byte[] bytes, int bytesPerLine = 16)
        {
            if (bytes == null) return "<null>";
            int bytesLength = bytes.Length;

            char[] HexChars = "0123456789ABCDEF".ToCharArray();

            int firstHexColumn =
                  8                   // 8 characters for the address
                + 3;                  // 3 spaces

            int firstCharColumn = firstHexColumn
                + bytesPerLine * 3       // - 2 digit for the hexadecimal value and 1 space
                + (bytesPerLine - 1) / 8 // - 1 extra space every 8 characters from the 9th
                + 2;                  // 2 spaces 

            int lineLength = firstCharColumn
                + bytesPerLine           // - characters to show the ascii value
                + Environment.NewLine.Length; // Carriage return and line feed (should normally be 2)

            char[] line = (new String(' ', lineLength - 2) + Environment.NewLine).ToCharArray();
            int expectedLines = (bytesLength + bytesPerLine - 1) / bytesPerLine;
            StringBuilder result = new StringBuilder(expectedLines * lineLength);

            for (int i = 0; i < bytesLength; i += bytesPerLine)
            {
                line[0] = HexChars[(i >> 28) & 0xF];
                line[1] = HexChars[(i >> 24) & 0xF];
                line[2] = HexChars[(i >> 20) & 0xF];
                line[3] = HexChars[(i >> 16) & 0xF];
                line[4] = HexChars[(i >> 12) & 0xF];
                line[5] = HexChars[(i >> 8) & 0xF];
                line[6] = HexChars[(i >> 4) & 0xF];
                line[7] = HexChars[(i >> 0) & 0xF];

                int hexColumn = firstHexColumn;
                int charColumn = firstCharColumn;

                for (int j = 0; j < bytesPerLine; j++)
                {
                    if (j > 0 && (j & 7) == 0) hexColumn++;
                    if (i + j >= bytesLength)
                    {
                        line[hexColumn] = ' ';
                        line[hexColumn + 1] = ' ';
                        line[charColumn] = ' ';
                    }
                    else
                    {
                        byte b = bytes[i + j];
                        line[hexColumn] = HexChars[(b >> 4) & 0xF];
                        line[hexColumn + 1] = HexChars[b & 0xF];
                        line[charColumn] = (b < 32 ? '·' : (char)b);
                    }
                    hexColumn += 3;
                    charColumn++;
                }
                result.Append(line);
            }
            return result.ToString();
        }
        /// <summary>
        /// Replaces 5 special characters in a string (&lt; &gt; &apos; &amp; &quot;) with their 
        /// xml entity equivalents, making the string safe to output.
        /// </summary>
        /// <param name="s">The string to be escaped</param>
        /// <returns>s with special characters replaced by entities</returns>
        public static string XmlEscape(string s)
        {
            if (s == null) { return null; }

            var b = new StringBuilder();
            foreach (char c in s)
            {
                switch (c)
                {
                    case '<':
                        b.Append("&lt;");
                        break;
                    case '>':
                        b.Append("&gt;");
                        break;
                    case '\'':
                        b.Append("&apos;");
                        break;
                    case '&':
                        b.Append("&amp;");
                        break;
                    case '"':
                        b.Append("&quot;");
                        break;
                    default:
                        b.Append(c);
                        break;
                }
            }
            return b.ToString();
        }

        /// <summary>
        /// Returns the first (at most) 64K characters from value
        /// </summary>
        /// <param name="s">The string input</param>
        /// <returns>The whole string if it is less than 64K characters long or just the first 64K characters</returns>
        internal static string First64K(string s)
        {
            const Int32 k64=65536;
            if (s==null || s.Length <= k64)
            {
                return s;
            }
            else
            {
                return s.Substring(0, k64);
            }
        }

        /// <summary>
        /// Returns an ISO8601 timestamp for the current time
        /// </summary>
        /// <remarks>Code inspired by http://stackoverflow.com/questions/114983/given-a-datetime-object-how-do-i-get-a-iso-8601-date-in-string-format answer by Sumrak</remarks>
        /// <returns>an ISO8601 timestamp for the current time</returns>
        public static string NowTimestamp()
        {
            return XmlConvert.ToString(DateTimeOffset.Now);
        }
    }
}
