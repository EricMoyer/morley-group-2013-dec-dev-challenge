﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class StationConfigDialog : Form
    {
        public UInt64 nextOrderNumber
        {
            get { return UInt64.Parse(nextOrderNumberTextBox.Text); }
            set { nextOrderNumberTextBox.Text = value.ToString(); }
        }

        public UInt64 storeNumber
        {
            get { return UInt64.Parse(storeNumberTextBox.Text); }
            set { storeNumberTextBox.Text = value.ToString(); }
        }

        public UInt64 tableNumber
        {
            get { return UInt64.Parse(tableNumberTextBox.Text); }
            set { tableNumberTextBox.Text = value.ToString(); }
        }


        public UInt64 terminalNumber
        {
            get { return UInt64.Parse(terminalNumberTextBox.Text); }
            set { terminalNumberTextBox.Text = value.ToString(); }
        }

        public Decimal taxRate
        {
            get { return Decimal.Parse(taxRateTextBox.Text)/100m; }
            set { taxRateTextBox.Text = (value*100m).ToString(); }
        }

        public string cashDrawer
        {
            get { return cashDrawerTextBox.Text; }
            set { cashDrawerTextBox.Text = value; }
        }

        public string notes
        {
            get { return notesTextBox.Text; }
            set { notesTextBox.Text = value; }
        }

        public StationConfigDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// If box.Text is a valid UInt64, color its accompanying label black. Otherwise, set DialogResult to None and color the label red to indicate an error.
        /// </summary>
        /// <param name="box">A text box that should contain a valid UInt64</param>
        /// <param name="label">A label associated with box that should be colored black/red depending on whether the contents of box are valid or not</param>
        private void validateUInt64TextBox(TextBox box, Label label)
        {
            try
            {
                UInt64.Parse(box.Text);
                label.ForeColor = Color.Black;
            }
            catch (FormatException)
            {
                DialogResult = DialogResult.None;
                label.ForeColor = Color.Red;
            }
            catch (OverflowException)
            {
                DialogResult = DialogResult.None;
                label.ForeColor = Color.Red;
            }

        }

        /// <summary>
        /// Validate the input and accept if OK
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void okButton_Click(object sender, EventArgs e)
        {

            validateUInt64TextBox(nextOrderNumberTextBox, nextOrderNumberLabel);
            validateUInt64TextBox(storeNumberTextBox, storeNumberLabel);
            validateUInt64TextBox(terminalNumberTextBox, terminalNumberLabel);
            validateUInt64TextBox(tableNumberTextBox, tableNumberLabel);

            try
            {
                Decimal tr = Decimal.Parse(taxRateTextBox.Text);
                if (tr < 0 || tr > 100) { throw new OverflowException("Tax rate is not in between 0 and 100%."); }
                taxRateLabel.ForeColor = Color.Black;
            }
            catch (FormatException)
            {
                DialogResult = DialogResult.None;
                taxRateLabel.ForeColor = Color.Red;
            }
            catch (OverflowException)
            {
                DialogResult = DialogResult.None;
                taxRateLabel.ForeColor = Color.Red;
            }


            if (cashDrawerTextBox.Text.Trim().Length == 0)
            {
                DialogResult = DialogResult.None;
                cashDrawerLabel.ForeColor = Color.Red;
            }
            else
            {
                cashDrawerLabel.ForeColor = Color.Black;
            }

        }
    }
}
