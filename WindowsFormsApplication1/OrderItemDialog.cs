﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class OrderItemDialog : Form
    {
        public string description { 
            get { return descriptionTextBox.Text; }
            set { descriptionTextBox.Text = value; }
        }

        public Decimal price { 
            get { return 100m*Decimal.Parse(priceTextBox.Text); }
            set { priceTextBox.Text = (value/100m).ToString(); }
        }
        public string notes { 
            get { return notesTextBox.Text; }
            set { notesTextBox.Text = value; }
        }
        public OrderItemDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Validate the input and accept if OK
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void okButton_Click(object sender, EventArgs e)
        {
            if (descriptionTextBox.Text.Trim().Length == 0)
            {
                DialogResult = DialogResult.None;
                descriptionLabel.ForeColor = Color.Red;
            }
            else
            {
                descriptionLabel.ForeColor = Color.Black;
            }

            try
            {
                var p = price;
                if (p > (Decimal)UInt32.MaxValue || p < (Decimal)UInt32.MinValue) { throw new FormatException("Price too large or small"); }
                priceLabel.ForeColor = Color.Black;
            }
            catch(FormatException)
            {
                DialogResult = DialogResult.None;
                priceLabel.ForeColor = Color.Red;
            }
        }
    }
}
