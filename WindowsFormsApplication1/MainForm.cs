﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace WindowsFormsApplication1
{
    public partial class MainForm : Form
    {
        /// <summary>
        /// The station represented by this form
        /// </summary>
        private Station station;

        /// <summary>
        /// Makes the control properly transparent by changing its parent and its coordinates. (From: http://stackoverflow.com/questions/9387267/transparent-background-label-over-picturebox)
        /// </summary>
        /// <param name="c">The control to make transparent</param>
        /// <param name="newParent">The picture-box over which the control sits</param>
        /// <param name="alpha">The alpha value to be used in setting the color. Same as Color.FromArgb(int, Color). Defaults to 0 (fully transparent)</param>
        void makeControlTransparent(System.Windows.Forms.Control c, PictureBox newParent, Int32 alpha=0){
            var pos = this.PointToScreen(c.Location);
            pos = newParent.PointToClient(pos);
            c.Parent = newParent;
            c.Location = pos;
            c.BackColor = Color.FromArgb(alpha, Color.White);
        }

        public MainForm()
        {
            InitializeComponent();

            InitStationUsingFields();

            logoutButton_Click(null, null); //Display login dialog first thing on startup

            // Fix description widths
            descriptionColumn.Width = 152;
            priceColumn.Width = 70;
            notesColumn.Width = 445;

            // Enable attribution link
            backgroundAttributionLinkLabel.Links.Add(0, backgroundAttributionLinkLabel.Text.Length, "http://www.flickr.com/photos/11939863@N08/1197112406/");

            // Fix designer+transparency problem
            makeControlTransparent(skyBurgerLabel, skyBackgroundPictureBox);
            makeControlTransparent(seatNumberLabel, skyBackgroundPictureBox);
            makeControlTransparent(toGoCheckBox, skyBackgroundPictureBox);
            makeControlTransparent(subtotalLabel, skyBackgroundPictureBox,128);
            makeControlTransparent(taxLabel, skyBackgroundPictureBox,128);
            makeControlTransparent(orderTotalLabel, skyBackgroundPictureBox,128);
            makeControlTransparent(backgroundAttributionLinkLabel, skyBackgroundPictureBox);

        }

        /// <summary>
        /// Use the values of the seatNumberTextBox and toGoCheckbox to create a blank station
        /// </summary>
        private void InitStationUsingFields()
        {
            Boolean isToGo = toGoCheckBox.Checked;
            station = new WindowsFormsApplication1.Station(parseSeatNumber(), isToGo);
        }

        /// <summary>
        /// Simple method for showing how to use the Encoding class which is part of the System.Text namespace
        /// </summary>
        void TestEncoding()
        {
            string myTestData = @"this is my test string";

            byte[] testdataBytes = Encoding.ASCII.GetBytes( myTestData );

            Debug.WriteLine( Utils.HexDump( testdataBytes ) );

            string convertedBytes = Encoding.ASCII.GetString( testdataBytes );

            Debug.WriteLine( convertedBytes );
        }

        private void skyBurgerLabel_Click(object sender, EventArgs e)
        {
            MessageBox.Show("XML:\n" + station.OrderXML());
        }

        private void toGoCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            station.order.isToGoOrder = toGoCheckBox.Checked;
        }

        /// <summary>
        /// Returns the seat number in the seatNumberTextBox. If the value in the textBox is not a valid seatNumber, sets it to one.
        /// </summary>
        /// <returns>The seat number in the seatNumberTextBox</returns>
        private UInt32 parseSeatNumber()
        {
            UInt32 seatNumber;
            try
            {
                seatNumber = UInt32.Parse(seatNumberTextBox.Text);
            }
            catch (FormatException)
            {
                seatNumber = 7; seatNumberTextBox.Text = "7";
            }
            return seatNumber;
        }
        private void seatNumberTextBox_TextChanged(object sender, EventArgs e)
        {
            station.order.seatNumber = parseSeatNumber();
        }

        /// <summary>
        /// Updates the subtotals, etc fields
        /// </summary>
        private void updateTotals()
        {
            Decimal subtotal = ((Decimal)station.order.Subtotal()) / 100;
            subtotalLabel.Text = "Subtotal: " + subtotal.ToString("C");

            Decimal tax = ((Decimal)station.order.Tax(station.taxRate)) / 100;
            taxLabel.Text = "Tax: " + tax.ToString("C");

            Decimal total = ((Decimal)station.order.Total(station.taxRate)) / 100;
            orderTotalLabel.Text = "Total: " + total.ToString("C");
        }

        private void addItem(string description, UInt32 price, string notes)
        {
            var i = new OrderItem(description, price, notes);
            Decimal dollarsPrice = price; dollarsPrice = dollarsPrice / 100;
            var lvi = new ListViewItem(new []{description, dollarsPrice.ToString("C"),notes});
            lvi.Tag = i;
            station.order.items.Add(i);
            orderItemsListView.Items.Add(lvi);
            updateTotals();
        }

        private void hamburgerButton_Click(object sender, EventArgs e)
        {
            addItem("Hamburger", 99, "");
        }

        private void thunderBurgerButton_Click(object sender, EventArgs e)
        {
            addItem("Thunder Burger", 595, "");
        }

        private void thunderBurgerWithCheeseButton_Click(object sender, EventArgs e)
        {
            addItem("Thunder Burger w/Cheese", 695, "");

        }

        private void specialOrderButton_Click(object sender, EventArgs e)
        {
            var dlg = new OrderItemDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                addItem(dlg.description, (uint)(dlg.price), dlg.notes);
            }
        }

        private void submitOrderButton_Click(object sender, EventArgs eventArgs)
        {
            // Notify user that doing something
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();

            // Do the post
            try
            {
                var helper = new TMG.Web.WebHelper();
                station.PostOrder(helper);
                if (station.order.items.Count == 0)
                {
                    // On successful post, 
                    orderItemsListView.Items.Clear();
                    updateTotals();
                    try
                    {
                        station.SaveConfig(); //The order number has changed - this writes it to the config file
                    }
                    catch (System.Configuration.ConfigurationErrorsException e) {
                        MessageBox.Show("Could not save new order number to the station configuration file. (" + e.Message + "(");
                    } // Do nothing if we couldn't save

                }
                // Let the user know app is not busy any more
                Cursor.Current = Cursors.Default;
                Application.DoEvents();
            }
            catch (System.IO.IOException e)
            {
                // Let the user know app is not busy any more
                Cursor.Current = Cursors.Default;
                Application.DoEvents();
                MessageBox.Show(e.Message);
            }

        }

        private void meteorFriesButton_Click(object sender, EventArgs e)
        {
            addItem("Meteor Fries", 99, "");
        }

        private void asteroidFriesButton_Click(object sender, EventArgs e)
        {
            addItem("Asteroid Fries", 149, "");
        }

        private void planetFriesButton_Click(object sender, EventArgs e)
        {
            addItem("Planet Fries", 199, "");
        }

        private void sodaDrizzleButton_Click(object sender, EventArgs e)
        {
            addItem("Soda Drizzle", 129, "");
        }

        private void sodaStormButton_Click(object sender, EventArgs e)
        {
            addItem("Soda Storm", 159, "");
        }

        private void sodaHurricaneButton_Click(object sender, EventArgs e)
        {
            addItem("Soda Hurricane", 299, "");
        }

        private void skyFlurryButton_Click(object sender, EventArgs e)
        {
            addItem("Sky Flurry", 199, "");
        }

        private void skyBlizzardButton_Click(object sender, EventArgs e)
        {
            addItem("Sky Blizzard", 299, "");
        }

        private void configButton_Click(object sender, EventArgs eventArgs)
        {
            var dlg = new StationConfigDialog();

            // Initialize dialog values
            dlg.cashDrawer = station.cashDrawer;
            dlg.nextOrderNumber = station.nextOrderNumber;
            dlg.notes = station.notes;
            dlg.storeNumber = station.storeNumber;
            dlg.tableNumber = station.tableNumber;
            dlg.taxRate = station.taxRate;
            dlg.terminalNumber = station.terminalNumber;


            // Set the values on successful completion
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                station.cashDrawer = dlg.cashDrawer;
                station.nextOrderNumber = dlg.nextOrderNumber;
                station.notes = dlg.notes;
                station.storeNumber = dlg.storeNumber;
                station.tableNumber = dlg.tableNumber;
                station.taxRate = dlg.taxRate;
                station.terminalNumber = dlg.terminalNumber;
                try
                {
                    station.SaveConfig(); 
                }
                catch (System.Configuration.ConfigurationErrorsException e)
                {
                    MessageBox.Show("Could not save station configuration. Changes will only be present for this session. (" + e.Message + ")");
                }

                updateTotals();
            }

        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            var dlg = new LoginDialog();
            dlg.associateID = station.associateID;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                station.associateID = dlg.associateID;
                try
                {
                    station.SaveConfig();
                }
                catch (System.Configuration.ConfigurationErrorsException)
                { //User doesn't need to worry if associate ID change doesn't persist, so ignore it
                }
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (orderItemsListView.SelectedItems.Count == 1)
            {
                var selected = orderItemsListView.SelectedItems[0];
                var curOrderItem = (OrderItem)selected.Tag;
                
                var dlg = new OrderItemDialog();
                dlg.description = curOrderItem.description;
                dlg.price = (Decimal)curOrderItem.price;
                dlg.notes = curOrderItem.notes;

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    curOrderItem.price = (UInt32) dlg.price;
                    curOrderItem.description = dlg.description;
                    curOrderItem.notes = dlg.notes;

                    selected.SubItems.Clear();
                    selected.Text = curOrderItem.description;
                    Decimal dollarsPrice = curOrderItem.price; dollarsPrice = dollarsPrice / 100;
                    selected.SubItems.Add(new ListViewItem.ListViewSubItem(selected, dollarsPrice.ToString("C")));
                    selected.SubItems.Add(new ListViewItem.ListViewSubItem(selected, curOrderItem.notes));

                    updateTotals();
                }
            }
            orderItemsListView.SelectedItems.Clear();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            // Copy the selected items so not iterating over a collection that is being modified
            var selected = new ListViewItem[orderItemsListView.SelectedItems.Count];
            orderItemsListView.SelectedItems.CopyTo(selected, 0);

            // Delete each selected item
            foreach (ListViewItem lvi in selected)
            {
                station.order.items.Remove((OrderItem)lvi.Tag);
                orderItemsListView.Items.Remove(lvi);
            }
            updateTotals();
            orderItemsListView.SelectedItems.Clear();
        }

        private void backgroundAttributionLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Mark link as visited and open it in the browser
            backgroundAttributionLinkLabel.LinkVisited = true; 
            System.Diagnostics.Process.Start(e.Link.LinkData.ToString());
        }
    }
}
