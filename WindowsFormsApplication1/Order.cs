﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Order
    {
        /// <summary>
        /// The number of the seat for the customer making the order.
        /// </summary>
        public UInt32 seatNumber { get; set; }

        /// <summary>
        /// True if the order is "to go", false otherwise.
        /// </summary>
        public Boolean isToGoOrder { get; set; }

        /// <summary>
        /// The items ordered by the customer
        /// </summary>
        public List<OrderItem> items { get; set; }

        /// <summary>
        /// Create an empty order for the given seat number and to-go status
        /// </summary>
        /// <param name="seatNumber">The number of the seat for the customer making the order.</param>
        /// <param name="isToGoOrder">True if the order is "to go", false otherwise.</param>
        public Order(UInt32 seatNumber, Boolean isToGoOrder)
        {
            this.seatNumber = seatNumber;
            this.isToGoOrder = isToGoOrder;
            this.items = new List<OrderItem>();
        }

        /// <summary>
        /// Return the XML for the items in this order - suitable for putting in the &lt;OrderItems&gt;...&lt;/OrderItems&gt; subtree in XML that will represent the order. 
        /// </summary>
        /// <remarks>Depends on OrderItem to properly escape its strings. Does not include the surrounding &lt;OrderItems&gt;...&lt;/OrderItems&gt; tag</remarks>
        /// <returns>Return the XML for the items in this order</returns>
        public string OrderItemsXML()
        {
            var xml = new StringBuilder();
            for(Int32 sequenceNumber = 1; sequenceNumber <= items.Count; ++sequenceNumber){
                OrderItem i = items[sequenceNumber-1];
                xml.Append(i.Xml(seatNumber, isToGoOrder, (UInt32)sequenceNumber));
            }
            return xml.ToString();
        }

        /// <summary>
        /// Return the total of the prices from the items ordered. 
        /// </summary>
        /// <remarks>Returns an amount in pennies. Will be non-negative. Using BigInteger is overkill. I originally designed it this way so that overflow would be impossible. However, to fill a 64 bit integer with pennies requires quadrillions of dollars - so a ulong would have been fine. However, when I realized that, I'd already written the BigInteger code.</remarks>
        /// <returns>The total of the prices from the items ordered.</returns>
        public BigInteger Subtotal()
        {
            BigInteger sum = 0;
            foreach (OrderItem i in items)
            {
                sum += i.price;
            }
            return sum;
        }

        /// <summary>
        /// Return the amount of tax to be charged at the given tax rate on the items in the list. 
        /// </summary>
        /// <remarks>An amount in pennies, rounded up. Will be non-negative. Using BigInteger is overkill. I originally designed it this way so that overflow would be impossible. However, to fill a 64 bit integer with pennies requires quadrillions of dollars - so a ulong would have been fine. However, when I realized that, I'd already written the BigInteger code.</remarks>
        /// <param name="taxRate">The tax rate as a fraction. Must be non-negative.</param>
        /// <returns>the amount of tax to be charged at the given tax rate on the items in the list.</returns>
        /// <example>To get the amount of tax at 10%, you would call: myOrder.Tax(0.1)</example>
        public BigInteger Tax(Decimal taxRate)
        {
            // Ensure tax rate is non-negative
            if(taxRate < 0m){ throw new ArgumentException("Negative tax rates are not allowed");}

            // Change the tax rate into a fraction where the denominator is a power of 10. All Decimal objects can be changed to this format losslessly based on their internal representation.
            Int32[] taxRateBits = Decimal.GetBits(taxRate);
            Byte scale = (Byte) ((taxRateBits[3] >> 16) & 0x7F);
            var numerator   = new BigInteger(new Decimal(taxRateBits[0],taxRateBits[1],taxRateBits[2],false,0));
            var denominator = new BigInteger(10); 
            denominator = BigInteger.Pow(denominator, scale);

            // Multiply that fraction by the subtotal
            BigInteger remainder;
            BigInteger tax = BigInteger.DivRem(Subtotal() * numerator, denominator, out remainder);

            // If the fractional part is greater than 1/2, round up
            if (remainder > 0 && remainder >= denominator / 2)
            {
                tax += 1;
            }

            return tax;
        }

        /// <summary>
        /// Return the sum of Subtotal() and Tax(taxRate). <see cref="Order.Subtotal()"/><see cref="Order.Tax(Decimal)"/>
        /// </summary>
        /// <remarks>Using BigInteger is overkill. I originally designed it this way so that overflow would be impossible. However, to fill a 64 bit integer with pennies requires quadrillions of dollars - so a ulong would have been fine. However, when I realized that, I'd already written the BigInteger code.</remarks>
        /// <param name="taxRate">The tax rate as a fraction. <see cref="Order.Tax(Decimal)"/></param>
        /// <returns>The sum of Subtotal() and Tax(taxRate)</returns>
        public BigInteger Total(Decimal taxRate)
        {
            return Subtotal() + Tax(taxRate);
        }
    }
}
