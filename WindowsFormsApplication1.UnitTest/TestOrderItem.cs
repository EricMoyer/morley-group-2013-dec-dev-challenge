﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WindowsFormsApplication1;
using System.Text;

namespace WindowsFormsApplication1.UnitTest
{
    [TestClass]
    public class TestOrderItemConstructor
    {
        /// <summary>
        /// Call constructor with nulls for both input strings
        /// </summary>
        [TestMethod]
        public void TestNulls()
        {
            var o = new OrderItem(null, 0, null);
            Assert.AreEqual("", o.description);
            Assert.AreEqual(0u, o.price);
            Assert.AreEqual("", o.notes);
        }

        /// <summary>
        /// Call constructor with empty strings for both input strings
        /// </summary>
        [TestMethod]
        public void TestEmpty()
        {
            var o = new OrderItem("", 1, "");
            Assert.AreEqual("", o.description);
            Assert.AreEqual(1u, o.price);
            Assert.AreEqual("", o.notes);
        }

        /// <summary>
        /// Call constructor with non-empty but short strings
        /// </summary>
        [TestMethod]
        public void TestShortStrings()
        {
            var o = new OrderItem("Some description", UInt32.MaxValue, "a note");
            Assert.AreEqual("Some description", o.description);
            Assert.AreEqual(UInt32.MaxValue, o.price);
            Assert.AreEqual("a note", o.notes);
        }

        /// <summary>
        /// Call constructor with strings longer than 64K.
        /// </summary>
        [TestMethod]
        public void TestLongStrings()
        {
            // Generate long strings and their properly truncated counterpart
            // D stands for description and N stands for notes
            var inputD = new StringBuilder();
            var expectedD = new StringBuilder();
            var inputN = new StringBuilder();
            var expectedN = new StringBuilder();
            for (int i = 0; i < 70000; ++i)
            {
                inputD.Append((i*3) % 10);
                inputN.Append((i*7) % 10);
                if (i < 65536)
                {
                    expectedD.Append((i*3) % 10);
                    expectedN.Append((i*7) % 10);
                }
            }

            var o = new OrderItem(inputD.ToString(), 15, inputN.ToString());
            Assert.AreEqual(expectedD.ToString(), o.description);
            Assert.AreEqual(15u, o.price);
            Assert.AreEqual(expectedN.ToString(), o.notes);
        }
    }

    [TestClass]
    public class TestOrderItemDescription
    {
        /// <summary>
        /// Set description with a null
        /// </summary>
        [TestMethod]
        public void TestNulls()
        {
            var o = new OrderItem("foo", 27, "bar");
            o.description = null;
            Assert.AreEqual("", o.description);
        }

        /// <summary>
        /// Set description with an empty string
        /// </summary>
        [TestMethod]
        public void TestEmpty()
        {
            var o = new OrderItem("foo", 27, "bar");
            o.description = "";
            Assert.AreEqual("", o.description);
        }

        /// <summary>
        /// Set description with a non-empty but short string
        /// </summary>
        [TestMethod]
        public void TestShortStrings()
        {
            var o = new OrderItem("foo", 27, "bar");
            o.description = "Something short and sweet";
            Assert.AreEqual("Something short and sweet", o.description);
        }

        /// <summary>
        /// Set description with a string longer than 64K.
        /// </summary>
        [TestMethod]
        public void TestLongStrings()
        {
            var input = new StringBuilder();
            var expected = new StringBuilder();
            for (int i = 0; i < 65537; ++i)
            {
                input.Append((i + 1) % 10);
                if (i < 65536)
                {
                    expected.Append((i + 1) % 10);
                }
            }

            var o = new OrderItem("foo", 27, "bar");
            o.description = input.ToString();
            Assert.AreEqual(expected.ToString(), o.description);
        }
    }

    [TestClass]
    public class TestOrderItemNotes
    {
        /// <summary>
        /// Set notes with a null
        /// </summary>
        [TestMethod]
        public void TestNulls()
        {
            var o = new OrderItem("foo", 27, "bar");
            o.notes = null;
            Assert.AreEqual("", o.notes);
        }

        /// <summary>
        /// Set notes with an empty string
        /// </summary>
        [TestMethod]
        public void TestEmpty()
        {
            var o = new OrderItem("foo", 27, "bar");
            o.notes = "";
            Assert.AreEqual("", o.notes);
        }

        /// <summary>
        /// Set notes with a non-empty but short string
        /// </summary>
        [TestMethod]
        public void TestShortStrings()
        {
            var o = new OrderItem("foo", 27, "bar");
            o.notes = "Something short!";
            Assert.AreEqual("Something short!", o.notes);
        }

        /// <summary>
        /// Set notes with a string longer than 64K.
        /// </summary>
        [TestMethod]
        public void TestLongStrings()
        {
            var input = new StringBuilder();
            var expected = new StringBuilder();
            for (int i = 0; i < 90000; ++i)
            {
                input.Append((i*7 + 1) % 10);
                if (i < 65536)
                {
                    expected.Append((i*7 + 1) % 10);
                }
            }

            var o = new OrderItem("foo", 27, "bar");
            o.notes = input.ToString();
            Assert.AreEqual(expected.ToString(), o.notes);
        }
    }

    [TestClass]
    public class TestOrderItemXML
    {
        /// <summary>
        /// Call xml with normal input values
        /// </summary>
        [TestMethod]
        public void TestNormal()
        {
            var o = new OrderItem("Thunder Burger", 595, "");
            string actual = o.Xml(25, true, 2);
            string expected = "<OrderItem><SequenceNumber>2</SequenceNumber><Description>Thunder Burger</Description><Price>595</Price><Notes></Notes><SeatNumber>25</SeatNumber><Quantity>1</Quantity><Togo>1</Togo></OrderItem>";
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Call xml with maximum input values
        /// </summary>
        [TestMethod]
        public void TestMax()
        {
            var o = new OrderItem("Thunder Burger", UInt32.MaxValue, "No mayo");
            string actual = o.Xml(UInt32.MaxValue, true, UInt32.MaxValue);
            string expected = "<OrderItem><SequenceNumber>4294967295</SequenceNumber><Description>Thunder Burger</Description><Price>4294967295</Price><Notes>No mayo</Notes><SeatNumber>4294967295</SeatNumber><Quantity>1</Quantity><Togo>1</Togo></OrderItem>";
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// Call xml with minimum input values
        /// </summary>
        [TestMethod]
        public void TestMin()
        {
            var o = new OrderItem("Thunder Burger", UInt32.MinValue, "No mayo");
            string actual = o.Xml(UInt32.MinValue, false, UInt32.MinValue);
            string expected = "<OrderItem><SequenceNumber>0</SequenceNumber><Description>Thunder Burger</Description><Price>0</Price><Notes>No mayo</Notes><SeatNumber>0</SeatNumber><Quantity>1</Quantity><Togo>0</Togo></OrderItem>";
            Assert.AreEqual(expected, actual);
        }


        /// <summary>
        /// Call xml with strings needing escaping
        /// </summary>
        [TestMethod]
        public void TestEscaping()
        {
            var o = new OrderItem("The \"<!>@#$%^&*()'\" Burger", 595, "They call it \"<!>@#$%^&*()'\" because this a family establishment.");
            string actual = o.Xml(25, true, 2);
            string expected = "<OrderItem><SequenceNumber>2</SequenceNumber><Description>The &quot;&lt;!&gt;@#$%^&amp;*()&apos;&quot; Burger</Description><Price>595</Price><Notes>They call it &quot;&lt;!&gt;@#$%^&amp;*()&apos;&quot; because this a family establishment.</Notes><SeatNumber>25</SeatNumber><Quantity>1</Quantity><Togo>1</Togo></OrderItem>";
            Assert.AreEqual(expected, actual);
        }
    }
}
