﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WindowsFormsApplication1;
using System.Text;
using Microsoft.QualityTools.Testing.Fakes;

namespace WindowsFormsApplication1.UnitTest
{
    [TestClass]
    public class TestEscapeXML
    {
        /// <summary>
        /// Test with no special characters
        /// </summary>
        [TestMethod]
        public void StringWithNoSpecialCharacters()
        {
            string input = "The rain in spain falls mainly on the quick brown fox who jumps over the lazy dog. ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz. (?!@#$%^*)";
            Assert.AreEqual(input, Utils.XmlEscape(input));
        }

        /// <summary>
        /// Test with the empty string
        /// </summary>
        [TestMethod]
        public void EmpytString()
        {
            Assert.AreEqual("", Utils.XmlEscape(""));
        }

        /// <summary>
        /// Test with all the special characters
        /// </summary>
        [TestMethod]
        public void StringWithAllSpecialCharacters()
        {
            string input = "</OrderItem>''(&amp;)\"";
            string expected = "&lt;/OrderItem&gt;&apos;&apos;(&amp;amp;)&quot;";
            Assert.AreEqual(expected, Utils.XmlEscape(input));
        }

        /// <summary>
        /// Test that when passed a null, returns a null
        /// </summary>
        [TestMethod]
        public void NullString()
        {
            string input = null;
            Assert.AreEqual(input, Utils.XmlEscape(input));
        }
    }

    [TestClass]
    public class TestFirst64K
    {
        /// <summary>
        /// Test with the empty string
        /// </summary>
        [TestMethod]
        public void EmptyString()
        {
            Assert.AreEqual("", Utils.First64K(""));
        }

        /// <summary>
        /// Test with a string short enough that it won't be truncated.
        /// </summary>
        [TestMethod]
        public void ShortString()
        {
            string input = "I feel pretty";
            Assert.AreEqual(input, Utils.First64K(input));
        }

        /// <summary>
        /// Test with a string long enough that it needs to be truncated.
        /// </summary>
        [TestMethod]
        public void OverlongString()
        {
            var inputB = new StringBuilder();
            var expectedB = new StringBuilder();
            for (int i = 0; i < 70000; ++i)
            {
                inputB.Append(i % 10);
                if (i < 65536)
                {
                    expectedB.Append(i % 10);
                }
            }

            string input = inputB.ToString();
            string expected = expectedB.ToString();

            Assert.AreEqual(expected, Utils.First64K(input));
        }

        /// <summary>
        /// Test that when passed a null, returns a null
        /// </summary>
        [TestMethod]
        public void NullString()
        {
            string input = null;
            Assert.AreEqual(input, Utils.First64K(input));
        }
    }

    [TestClass]
    public class TestHexDump
    {
        /// <summary>
        /// Test basic functionality with a simple byte array.
        /// </summary>
        [TestMethod]
        public void SimpleDump()
        {
            var b = new byte[]{
                    1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
                    17,18,19,20,21,22,23,24,65};
            string expected =
                "00000000   01 02 03 04 05 06 07 08  09 0A 0B 0C 0D 0E 0F 10   ················" + Environment.NewLine +
                "00000010   11 12 13 14 15 16 17 18  41                        ········A       " + Environment.NewLine;
            Assert.AreEqual(expected, Utils.HexDump(b));
        }

        /// <summary>
        /// Test behavior when passed a null
        /// </summary>
        [TestMethod]
        public void PassedNull()
        {
            byte[] b = null;
            string expected = "<null>";
            Assert.AreEqual(expected, Utils.HexDump(b));
        }
    
    }

    [TestClass]
    public class TestNowTimestamp
    {
        /// <summary>
        /// Test whether the timestamp is correct when the DateTimeOffset.Now is forced to 5:30:24 on May 10, 2013
        /// </summary>
        [TestMethod]
        public void May2013()
        {
            using (ShimsContext.Create())
            {
                System.Fakes.ShimDateTimeOffset.NowGet =
                () =>
                { return new DateTimeOffset(2013, 5, 10, 5, 30, 24, new TimeSpan(-4,0,0)); };
                string expected = "2013-05-10T05:30:24-04:00";
                Assert.AreEqual(expected, Utils.NowTimestamp());
            }
        }

        /// <summary>
        /// Test whether the timestamp returned has a valid timezone
        /// </summary>
        [TestMethod]
        public void HasTimezone()
        {
            string ts = Utils.NowTimestamp();
            var pattern = new System.Text.RegularExpressions.Regex("[+-]??:??$");
            Assert.IsTrue(pattern.IsMatch(ts));
        }
    }
}
