﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;

namespace WindowsFormsApplication1.UnitTest
{
    [TestClass]
    public class TestOrderItemsXML
    {
        /// <summary>
        /// Test with an empty order
        /// </summary>
        [TestMethod]
        public void EmptyOrder()
        {
            var o = new Order(120, false);
            string expected = "";
            Assert.AreEqual(expected, o.OrderItemsXML());
        }

        /// <summary>
        /// Test with an order with a single item
        /// </summary>
        [TestMethod]
        public void SingleItem()
        {
            var o = new Order(121, true);
            o.items.Add(new OrderItem("Sky Flurry", 199, ""));
            string expected = "<OrderItem><SequenceNumber>1</SequenceNumber><Description>Sky Flurry</Description><Price>199</Price><Notes></Notes><SeatNumber>121</SeatNumber><Quantity>1</Quantity><Togo>1</Togo></OrderItem>";
            Assert.AreEqual(expected, o.OrderItemsXML());
        }

        /// <summary>
        /// Test with an order with three items
        /// </summary>
        [TestMethod]
        public void ThreeItems()
        {
            var o = new Order(122, false);
            o.items.Add(new OrderItem("Thunder Burger", 595, "Hold lightning sauce"));
            o.items.Add(new OrderItem("Soda Drizzle<>!", 99, ""));
            o.items.Add(new OrderItem("Planet Fries", 399, "With pepper & salt"));
            string expected =
                "<OrderItem><SequenceNumber>1</SequenceNumber><Description>Thunder Burger</Description><Price>595</Price><Notes>Hold lightning sauce</Notes><SeatNumber>122</SeatNumber><Quantity>1</Quantity><Togo>0</Togo></OrderItem>" +
                "<OrderItem><SequenceNumber>2</SequenceNumber><Description>Soda Drizzle&lt;&gt;!</Description><Price>99</Price><Notes></Notes><SeatNumber>122</SeatNumber><Quantity>1</Quantity><Togo>0</Togo></OrderItem>" +
                "<OrderItem><SequenceNumber>3</SequenceNumber><Description>Planet Fries</Description><Price>399</Price><Notes>With pepper &amp; salt</Notes><SeatNumber>122</SeatNumber><Quantity>1</Quantity><Togo>0</Togo></OrderItem>";
            Assert.AreEqual(expected, o.OrderItemsXML());
        }
    }

    [TestClass]
    public class TestSubtotal
    {
        /// <summary>
        /// Test with an empty order
        /// </summary>
        [TestMethod]
        public void EmptyOrder()
        {
            var o = new Order(123, true);
            Assert.AreEqual(0, o.Subtotal());
        }

        /// <summary>
        /// Test with an order with a single item
        /// </summary>
        [TestMethod]
        public void SingleItem()
        {
            var o = new Order(124, false);
            o.items.Add(new OrderItem("Sky Flurry", 199, ""));
            Assert.AreEqual(199, o.Subtotal());
        }

        /// <summary>
        /// Test with an order with three items
        /// </summary>
        [TestMethod]
        public void ThreeItems()
        {
            var o = new Order(125, true);
            o.items.Add(new OrderItem("Thunder Burger", 595, "Hold lightning sauce"));
            o.items.Add(new OrderItem("Soda Drizzle", 99, ""));
            o.items.Add(new OrderItem("Planet Fries", 399, "With pepper"));
            Assert.AreEqual(1093, o.Subtotal());
        }
    }

    /// <summary>
    /// Each test case in TestTax will test with 4 tax rates: negative (throws exception), zero, something that should round down, and something that should round up
    /// </summary>
    [TestClass]
    public class TestTax
    {
        /// <summary>
        /// Test with an empty order
        /// </summary>
        [TestMethod]
        public void EmptyOrder()
        {
            var o = new Order(126, false);
            Assert.AreEqual(new BigInteger(0), o.Tax(0m));
            Assert.AreEqual(new BigInteger(0), o.Tax(0.1m));
        }

        /// <summary>
        /// Test with an order with a single item
        /// </summary>
        [TestMethod]
        public void SingleItem()
        {
            var o = new Order(127, true);
            o.items.Add(new OrderItem("Sky Flurry", 199, ""));
            Assert.AreEqual(new BigInteger(0), o.Tax(0m));
            Assert.AreEqual(new BigInteger(1), o.Tax(0.007m)); // Rounds down
            Assert.AreEqual(new BigInteger(20), o.Tax(0.1m));  // Rounds up
        }

        /// <summary>
        /// Test with an order with three items
        /// </summary>
        [TestMethod]
        public void ThreeItems()
        {
            var o = new Order(128, false);
            o.items.Add(new OrderItem("Thunder Burger", 595, "Hold lightning sauce"));
            o.items.Add(new OrderItem("Soda Drizzle", 99, ""));
            o.items.Add(new OrderItem("Planet Fries", 399, "With pepper"));
            Assert.AreEqual(new BigInteger(0), o.Tax(0m));
            Assert.AreEqual(new BigInteger(109), o.Tax(0.1m)); // Rounds down
            Assert.AreEqual(new BigInteger(8), o.Tax(0.007m)); // Rounds up
        }

        /// <summary>
        /// Test with an empty order and a negative tax rate
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Negative tax rates are not allowed")]
        public void EmptyOrderNegTax()
        {
            var o = new Order(126, false);
            o.Tax(-0.1m);
        }

        /// <summary>
        /// Test with an order with a single item and a negative tax rate
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Negative tax rates are not allowed")]
        public void SingleItemNegTax()
        {
            var o = new Order(127, true);
            o.items.Add(new OrderItem("Sky Flurry", 199, ""));
            o.Tax(-1m);
        }

        /// <summary>
        /// Test with an order with three items and a negative tax rate
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Negative tax rates are not allowed")]
        public void ThreeItemsNegTax()
        {
            var o = new Order(128, false);
            o.items.Add(new OrderItem("Thunder Burger", 595, "Hold lightning sauce"));
            o.items.Add(new OrderItem("Soda Drizzle", 99, ""));
            o.items.Add(new OrderItem("Planet Fries", 399, "With pepper"));
            o.Tax(-0.000000000001m);
        }
    }

    /// <summary>
    /// Each test case in TestTotal will test with 4 tax rates: negative (throws exception), zero, something that should round down, and something that should round up
    /// </summary>
    [TestClass]
    public class TestTotal
    {
        /// <summary>
        /// Test with an empty order
        /// </summary>
        [TestMethod]
        public void EmptyOrder()
        {
            var o = new Order(126, false);
            Assert.AreEqual(new BigInteger(0), o.Total(0m));
            Assert.AreEqual(new BigInteger(0), o.Total(0.1m));
        }

        /// <summary>
        /// Test with an order with a single item
        /// </summary>
        [TestMethod]
        public void SingleItem()
        {
            var o = new Order(127, true);
            o.items.Add(new OrderItem("Sky Flurry", 199, ""));
            Assert.AreEqual(new BigInteger(199), o.Total(0m));
            Assert.AreEqual(new BigInteger(200), o.Total(0.007m)); // Rounds down
            Assert.AreEqual(new BigInteger(219), o.Total(0.1m));   // Rounds up
        }

        /// <summary>
        /// Test with an order with three items
        /// </summary>
        [TestMethod]
        public void ThreeItems()
        {
            var o = new Order(128, false);
            o.items.Add(new OrderItem("Thunder Burger", 595, "Hold lightning sauce"));
            o.items.Add(new OrderItem("Soda Drizzle", 99, ""));
            o.items.Add(new OrderItem("Planet Fries", 399, "With pepper"));
            Assert.AreEqual(new BigInteger(1093), o.Total(0m));
            Assert.AreEqual(new BigInteger(1202), o.Total(0.1m)); // Rounds down
            Assert.AreEqual(new BigInteger(1101), o.Total(0.007m)); // Rounds up
        }

        /// <summary>
        /// Test with an empty order and a negative tax rate
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Negative tax rates are not allowed")]
        public void EmptyOrderNegTax()
        {
            var o = new Order(126, false);
            o.Total(-0.5m);
        }

        /// <summary>
        /// Test with an order with a single item and a negative tax rate
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Negative tax rates are not allowed")]
        public void SingleItemNegTax()
        {
            var o = new Order(127, true);
            o.items.Add(new OrderItem("Sky Flurry", 199, ""));
            o.Total(-1m);
        }

        /// <summary>
        /// Test with an order with three items and a negative tax rate
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Negative tax rates are not allowed")]
        public void ThreeItemsNegTax()
        {
            var o = new Order(128, false);
            o.items.Add(new OrderItem("Thunder Burger", 595, "Hold lightning sauce"));
            o.items.Add(new OrderItem("Soda Drizzle", 99, ""));
            o.items.Add(new OrderItem("Planet Fries", 399, "With pepper"));
            o.Total(-0.0001m);
        }
    }
}
