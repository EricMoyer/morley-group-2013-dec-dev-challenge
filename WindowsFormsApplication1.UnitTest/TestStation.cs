﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using Microsoft.QualityTools.Testing.Fakes;
using System.Configuration;
using System.Diagnostics;
using System.Net;

namespace WindowsFormsApplication1.UnitTest.TestStation
{

    [TestClass]
    public class LongPrivateConstructor
    {
        /// <summary>
        /// Run the private constructor with reasonable values
        /// </summary>
        /// <remarks>I'm leaving the unreasonable values to the individual 
        /// property tests. Not good test practice, but unit tests aren't
        /// even required for this project.</remarks>
        [TestMethod]
        public void SimpleRun()
        {
            // This is the information from the sample xml in the 
            // requirements document
            var o = new WindowsFormsApplication1.Order(1, false);
            var s = new Station("7C725161-B511-4C52-B050-44513107FB40",
                1200, 9908, 30, 8864, "NCRCashDrawer.2", 0.65m,
                "Table Notes: these are mikeys notes", o);
            Assert.AreEqual("7C725161-B511-4C52-B050-44513107FB40", s.associateID);
            Assert.AreEqual(1200ul, s.nextOrderNumber);
            Assert.AreEqual(9908ul, s.storeNumber);
            Assert.AreEqual(30ul, s.terminalNumber);
            Assert.AreEqual(8864ul, s.tableNumber);
            Assert.AreEqual("NCRCashDrawer.2", s.cashDrawer);
            Assert.AreEqual(0.65m, s.taxRate);
            Assert.AreEqual("Table Notes: these are mikeys notes", s.notes);
            Assert.AreEqual(o, s.order);
        }

    }

    [TestClass]
    public class AssociateID
    {
        /// <summary>
        /// Test handling of a null input
        /// </summary>
        [TestMethod]
        public void Null()
        {
            var s = new Station("myID", 12, 13, 14, 15, "drawer", 0.065m,
                "note", new WindowsFormsApplication1.Order(1, false));
            s.associateID = null;
            Assert.AreEqual("", s.associateID);
        }

        /// <summary>
        /// Test handling of an empty string
        /// </summary>
        [TestMethod]
        public void EmptyString()
        {
            var s = new Station("myID", 12, 13, 14, 15, "drawer", 0.065m,
                "note", new WindowsFormsApplication1.Order(1, false));
            s.associateID = "";
            Assert.AreEqual("", s.associateID);
        }

        /// <summary>
        /// Test handling of a normal string
        /// </summary>
        [TestMethod]
        public void NormalString()
        {
            var s = new Station("myID", 12, 13, 14, 15, "drawer", 0.065m,
                "note", new WindowsFormsApplication1.Order(1, false));
            s.associateID = "1234-5678-90ABCD";
            Assert.AreEqual("1234-5678-90ABCD", s.associateID);
        }

        /// <summary>
        /// Test handling of a string that is long enough that it should 
        /// be truncated
        /// </summary>
        [TestMethod]
        public void TooLong()
        {
            // Generate long string and its properly truncated counterpart
            var input = new StringBuilder();
            var expected = new StringBuilder();
            for (int i = 0; i < 66000; ++i)
            {
                input.Append((i * 7) % 10);
                if (i < 65536)
                {
                    expected.Append((i * 7) % 10);
                }
            }

            // Test handling of long string
            var s = new Station("myID", 12, 13, 14, 15, "drawer", 0.065m,
                "note", new WindowsFormsApplication1.Order(1, false));
            s.associateID = input.ToString();
            Assert.AreEqual(expected.ToString(), s.associateID);
        }
    }

    [TestClass]
    public class CashDrawer
    {
        /// <summary>
        /// Test handling of a null input
        /// </summary>
        [TestMethod]
        public void Null()
        {
            var s = new Station("myID", 12, 13, 14, 15, "drawer", 0.065m,
                "note", new WindowsFormsApplication1.Order(1, false));
            s.cashDrawer = null;
            Assert.AreEqual("", s.cashDrawer);
        }

        /// <summary>
        /// Test handling of an empty string
        /// </summary>
        [TestMethod]
        public void EmptyString()
        {
            var s = new Station("myID", 12, 13, 14, 15, "drawer", 0.065m,
                "note", new WindowsFormsApplication1.Order(1, false));
            s.cashDrawer = "";
            Assert.AreEqual("", s.cashDrawer);
        }

        /// <summary>
        /// Test handling of a normal string
        /// </summary>
        [TestMethod]
        public void NormalString()
        {
            var s = new Station("myID", 12, 13, 14, 15, "drawer", 0.065m,
                "note", new WindowsFormsApplication1.Order(1, false));
            s.cashDrawer = "1234-5678-90ABCD";
            Assert.AreEqual("1234-5678-90ABCD", s.cashDrawer);
        }

        /// <summary>
        /// Test handling of a string that is long enough that it should 
        /// be truncated
        /// </summary>
        [TestMethod]
        public void TooLong()
        {
            // Generate long string and its properly truncated counterpart
            var input = new StringBuilder();
            var expected = new StringBuilder();
            for (int i = 0; i < 66000; ++i)
            {
                input.Append((i * 7) % 10);
                if (i < 65536)
                {
                    expected.Append((i * 7) % 10);
                }
            }

            // Test handling of long string
            var s = new Station("myID", 12, 13, 14, 15, "drawer", 0.065m,
                "note", new WindowsFormsApplication1.Order(1, false));
            s.cashDrawer = input.ToString();
            Assert.AreEqual(expected.ToString(), s.cashDrawer);
        }
    }

    [TestClass]
    public class Notes
    {
        /// <summary>
        /// Test handling of a null input
        /// </summary>
        [TestMethod]
        public void Null()
        {
            var s = new Station("myID", 12, 13, 14, 15, "drawer", 0.065m,
                "note", new WindowsFormsApplication1.Order(1, false));
            s.notes = null;
            Assert.AreEqual("", s.notes);
        }

        /// <summary>
        /// Test handling of an empty string
        /// </summary>
        [TestMethod]
        public void EmptyString()
        {
            var s = new Station("myID", 12, 13, 14, 15, "drawer", 0.065m,
                "note", new WindowsFormsApplication1.Order(1, false));
            s.notes = "";
            Assert.AreEqual("", s.notes);
        }

        /// <summary>
        /// Test handling of a normal string
        /// </summary>
        [TestMethod]
        public void NormalString()
        {
            var s = new Station("myID", 12, 13, 14, 15, "drawer", 0.065m,
                "note", new WindowsFormsApplication1.Order(1, false));
            s.notes = "1234-5678-90ABCD";
            Assert.AreEqual("1234-5678-90ABCD", s.notes);
        }

        /// <summary>
        /// Test handling of a string that is long enough that it should 
        /// be truncated
        /// </summary>
        [TestMethod]
        public void TooLong()
        {
            // Generate long string and its properly truncated counterpart
            var input = new StringBuilder();
            var expected = new StringBuilder();
            for (int i = 0; i < 66000; ++i)
            {
                input.Append((i * 7) % 10);
                if (i < 65536)
                {
                    expected.Append((i * 7) % 10);
                }
            }

            // Test handling of long string
            var s = new Station("myID", 12, 13, 14, 15, "drawer", 0.065m,
                "note", new WindowsFormsApplication1.Order(1, false));
            s.notes = input.ToString();
            Assert.AreEqual(expected.ToString(), s.notes);
        }
    }

    [TestClass]
    public class NextOrderNumber
    {
        /// <summary>
        /// Test the minimum value for nextOrderNumber
        /// </summary>
        [TestMethod]
        public void Min()
        {
            var s = new Station("iD", 0, 1, 2, 3, "drawer", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.nextOrderNumber = UInt64.MinValue;
            Assert.AreEqual(UInt64.MinValue, s.nextOrderNumber);
        }

        /// <summary>
        /// Test the maximum value for nextOrderNumber
        /// </summary>
        [TestMethod]
        public void Max()
        {
            var s = new Station("iD", 0, 1, 2, 3, "drawer", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.nextOrderNumber = UInt64.MaxValue;
            Assert.AreEqual(UInt64.MaxValue, s.nextOrderNumber);
        }
    }

    [TestClass]
    public class StoreNumber
    {
        /// <summary>
        /// Test the minimum value for storeNumber
        /// </summary>
        [TestMethod]
        public void Min()
        {
            var s = new Station("iD", 0, 1, 2, 3, "drawer", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.storeNumber = UInt64.MinValue;
            Assert.AreEqual(UInt64.MinValue, s.storeNumber);
        }

        /// <summary>
        /// Test the maximum value for storeNumber
        /// </summary>
        [TestMethod]
        public void Max()
        {
            var s = new Station("iD", 0, 1, 2, 3, "drawer", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.storeNumber = UInt64.MaxValue;
            Assert.AreEqual(UInt64.MaxValue, s.storeNumber);
        }

    }

    [TestClass]
    public class TerminalNumber
    {
        /// <summary>
        /// Test the minimum value for terminalNumber
        /// </summary>
        [TestMethod]
        public void Min()
        {
            var s = new Station("iD", 0, 1, 2, 3, "drawer", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.terminalNumber = UInt64.MinValue;
            Assert.AreEqual(UInt64.MinValue, s.terminalNumber);
        }

        /// <summary>
        /// Test the maximum value for terminalNumber
        /// </summary>
        [TestMethod]
        public void Max()
        {
            var s = new Station("iD", 0, 1, 2, 3, "drawer", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.terminalNumber = UInt64.MaxValue;
            Assert.AreEqual(UInt64.MaxValue, s.terminalNumber);
        }
    }

    [TestClass]
    public class TableNumber
    {
        /// <summary>
        /// Test the minimum value for tableNumber
        /// </summary>
        [TestMethod]
        public void Min()
        {
            var s = new Station("iD", 0, 1, 2, 3, "drawer", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.tableNumber = UInt64.MinValue;
            Assert.AreEqual(UInt64.MinValue, s.tableNumber);
        }

        /// <summary>
        /// Test the maximum value for tableNumber
        /// </summary>
        [TestMethod]
        public void Max()
        {
            var s = new Station("iD", 0, 1, 2, 3, "drawer", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.tableNumber = UInt64.MaxValue;
            Assert.AreEqual(UInt64.MaxValue, s.tableNumber);
        }
    }

    [TestClass]
    public class TaxRate
    {
        /// <summary>
        /// Test the minimum value for taxRate
        /// </summary>
        [TestMethod]
        public void Min()
        {
            var s = new Station("iD", 0, 1, 2, 3, "drawer", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.taxRate = 0m;
            Assert.AreEqual(0m, s.taxRate);
        }

        /// <summary>
        /// Test the maximum value for taxRate
        /// </summary>
        [TestMethod]
        public void Max()
        {
            var s = new Station("iD", 0, 1, 2, 3, "drawer", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.taxRate = 1m;
            Assert.AreEqual(1m, s.taxRate);
        }

        /// <summary>
        /// Test a value smaller than the minimum taxRate
        /// </summary>
        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void TooSmall()
        {
            var s = new Station("iD", 0, 1, 2, 3, "drawer", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.taxRate = -0.00001m;
        }

        /// <summary>
        /// Test a value greater than the maximum taxRate
        /// </summary>
        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void TooLarge()
        {
            var s = new Station("iD", 0, 1, 2, 3, "drawer", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.taxRate = 1.1m;
        }
    }

    [TestClass]
    public class Order
    {
        /// <summary>
        /// Test assigning a normal Order to order property
        /// </summary>
        [TestMethod]
        public void Normal()
        {
            var s = new Station("OID", 0, 1, 2, 3, "drawer.2", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.order = new WindowsFormsApplication1.Order(10, false);
            Assert.AreEqual(false, s.order.isToGoOrder);
            Assert.AreEqual(10u, s.order.seatNumber);
            Assert.AreEqual(0, s.order.items.Count);
        }

        /// <summary>
        /// Test assigning a null to the order property
        /// </summary>
        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void Null()
        {
            var s = new Station("OID", 0, 1, 2, 3, "drawer.2", 0.65m,
                "", new WindowsFormsApplication1.Order(2, true));
            s.order = null;
        }
    }

    [TestClass]
    public class OrderXML
    {
        /// <summary>
        /// Test an order with no items
        /// </summary>
        [TestMethod]
        public void EmptyOrder()
        {
            var o = new WindowsFormsApplication1.Order(1, false);
            var s = new Station("7C725161-B511-4C52-B050-44513107FB40",
                1200, 9908, 30, 8864, "NCRCashDrawer.2", 0.065m,
                "Table Notes: these are mikeys notes", o);
            using (ShimsContext.Create())
            {
                System.Fakes.ShimDateTimeOffset.NowGet = () =>
                    { return new DateTimeOffset(2013, 5, 10, 5, 30, 24, new TimeSpan(-4,0,0)); };

                string expected = "<Order>" +
                    "<AssociateId>7C725161-B511-4C52-B050-44513107FB40</AssociateId>" +
                    "<Subtotal>0</Subtotal>" +
                    "<Tax>0</Tax>" +
                    "<Total>0</Total>" +
                    "<OrderNumber>1200</OrderNumber>" +
                    "<StoreNumber>9908</StoreNumber>" +
                    "<TerminalNumber>30</TerminalNumber>" +
                    "<TableNumber>8864</TableNumber><Printer/>" +
                    "<CashDrawer>NCRCashDrawer.2</CashDrawer>" +
                    "<TaxRate>0.065</TaxRate><CustomerName/>" +
                    "<Notes>Table Notes: these are mikeys notes</Notes>" +
                    "<Timestamp>2013-05-10T05:30:24-04:00</Timestamp>" +
                    "<ModifiedTimestamp>2013-05-10T05:30:24-04:00</ModifiedTimestamp>" +
                    "<OrderItems>" +
                    "</OrderItems>" +
                    "</Order>";
                Assert.AreEqual(expected, s.OrderXML());
            }
        }

        /// <summary>
        /// Test an order with one item
        /// </summary>
        [TestMethod]
        public void OneItem()
        {
            var o = new WindowsFormsApplication1.Order(1, false);
            var s = new Station("7C725161-B511-4C52-B050-44513107FB40",
                1200, 9908, 30, 8864, "NCRCashDrawer.2", 0.065m,
                "Table Notes: these are mikeys notes", o);
            s.order.items.Add(new OrderItem("Steak Sub", 715, "this is mikeys item 1 notes"));
            using (ShimsContext.Create())
            {
                System.Fakes.ShimDateTimeOffset.NowGet = () =>
                { return new DateTimeOffset(2013, 5, 10, 5, 35, 24, new TimeSpan(-4, 0, 0)); };

                // this is a slight modification of the xml from the requirements document
                string expected = "<Order>" +
                    "<AssociateId>7C725161-B511-4C52-B050-44513107FB40</AssociateId>" +
                    "<Subtotal>715</Subtotal>" +
                    "<Tax>46</Tax>" +
                    "<Total>761</Total>" +
                    "<OrderNumber>1200</OrderNumber>" +
                    "<StoreNumber>9908</StoreNumber>" +
                    "<TerminalNumber>30</TerminalNumber>" +
                    "<TableNumber>8864</TableNumber><Printer/>" +
                    "<CashDrawer>NCRCashDrawer.2</CashDrawer>" +
                    "<TaxRate>0.065</TaxRate><CustomerName/>" +
                    "<Notes>Table Notes: these are mikeys notes</Notes>" +
                    "<Timestamp>2013-05-10T05:35:24-04:00</Timestamp>" +
                    "<ModifiedTimestamp>2013-05-10T05:35:24-04:00</ModifiedTimestamp>" +
                    "<OrderItems>" +
                        "<OrderItem>" +
                            "<SequenceNumber>1</SequenceNumber>" +
                            "<Description>Steak Sub</Description>" +
                            "<Price>715</Price>" +
                            "<Notes>this is mikeys item 1 notes</Notes>" +
                            "<SeatNumber>1</SeatNumber>" +
                            "<Quantity>1</Quantity>" +
                            "<Togo>0</Togo>" +
                        "</OrderItem>" +
                    "</OrderItems>" +
                    "</Order>";
                Assert.AreEqual(expected, s.OrderXML());
            }
        }

        /// <summary>
        /// Test an order with two items
        /// </summary>
        [TestMethod]
        public void TwoItems()
        {
            var o = new WindowsFormsApplication1.Order(1, true);
            var s = new Station("7C725161-B511-4C52-B050-44513107FB40",
                1200, 9908, 30, 8864, "NCRCashDrawer.2", 0.065m,
                "Table Notes: these are mikeys notes", o);
            s.order.items.Add(new OrderItem("Steak Sub", 715, "this is mikeys item 1 notes"));
            s.order.items.Add(new OrderItem("Steak Sub", 715, "this is mikeys item 2 notes"));
            using (ShimsContext.Create())
            {
                System.Fakes.ShimDateTimeOffset.NowGet = () =>
                    { return new DateTimeOffset(2013, 12, 10, 6, 35, 24, new TimeSpan(-7, 0, 0)); };

                // this is a slight modification of the xml from the requirements document
                string expected = "<Order>" +
                    "<AssociateId>7C725161-B511-4C52-B050-44513107FB40</AssociateId>" +
                    "<Subtotal>1430</Subtotal>" +
                    "<Tax>93</Tax>" +
                    "<Total>1523</Total>" +
                    "<OrderNumber>1200</OrderNumber>" +
                    "<StoreNumber>9908</StoreNumber>" +
                    "<TerminalNumber>30</TerminalNumber>" +
                    "<TableNumber>8864</TableNumber><Printer/>" +
                    "<CashDrawer>NCRCashDrawer.2</CashDrawer>" +
                    "<TaxRate>0.065</TaxRate><CustomerName/>" +
                    "<Notes>Table Notes: these are mikeys notes</Notes>" +
                    "<Timestamp>2013-12-10T06:35:24-07:00</Timestamp>" +
                    "<ModifiedTimestamp>2013-12-10T06:35:24-07:00</ModifiedTimestamp>" +
                    "<OrderItems>" +
                        "<OrderItem>" +
                            "<SequenceNumber>1</SequenceNumber>" +
                            "<Description>Steak Sub</Description>" +
                            "<Price>715</Price>" +
                            "<Notes>this is mikeys item 1 notes</Notes>" +
                            "<SeatNumber>1</SeatNumber>" +
                            "<Quantity>1</Quantity>" +
                            "<Togo>1</Togo>" +
                        "</OrderItem>" +
                        "<OrderItem>" +
                            "<SequenceNumber>2</SequenceNumber>" +
                            "<Description>Steak Sub</Description>" +
                            "<Price>715</Price>" +
                            "<Notes>this is mikeys item 2 notes</Notes>" +
                            "<SeatNumber>1</SeatNumber>" +
                            "<Quantity>1</Quantity>" +
                            "<Togo>1</Togo>" +
                        "</OrderItem>" +
                    "</OrderItems>" +
                    "</Order>";
                Assert.AreEqual(expected, s.OrderXML());
            }
        }

        /// <summary>
        /// Tests an order where the property strings in this station need escaping
        /// </summary>
        [TestMethod]
        public void StringsNeedEscaping()
        {
            var o = new WindowsFormsApplication1.Order(1, false);
            var s = new Station("7C725161-&B511&-<4C52>-'B050'-\"44513107FB40\"",
                1200, 9908, 30, 8864, "&<NCRCashDrawer.2>", 0.065m,
                "Table Notes: these are mikeys notes :-<>", o);
            using (ShimsContext.Create())
            {
                System.Fakes.ShimDateTimeOffset.NowGet = () =>
                    { return new DateTimeOffset(2013, 12, 10, 6, 35, 24, new TimeSpan(-7, 0, 0)); };

                string expected = "<Order>" +
                    "<AssociateId>7C725161-&amp;B511&amp;-&lt;4C52&gt;-&apos;B050&apos;-&quot;44513107FB40&quot;</AssociateId>" +
                    "<Subtotal>0</Subtotal>" +
                    "<Tax>0</Tax>" +
                    "<Total>0</Total>" +
                    "<OrderNumber>1200</OrderNumber>" +
                    "<StoreNumber>9908</StoreNumber>" +
                    "<TerminalNumber>30</TerminalNumber>" +
                    "<TableNumber>8864</TableNumber><Printer/>" +
                    "<CashDrawer>&amp;&lt;NCRCashDrawer.2&gt;</CashDrawer>" +
                    "<TaxRate>0.065</TaxRate><CustomerName/>" +
                    "<Notes>Table Notes: these are mikeys notes :-&lt;&gt;</Notes>" +
                    "<Timestamp>2013-12-10T06:35:24-07:00</Timestamp>" +
                    "<ModifiedTimestamp>2013-12-10T06:35:24-07:00</ModifiedTimestamp>" +
                    "<OrderItems>" +
                    "</OrderItems>" +
                    "</Order>";
                Assert.AreEqual(expected, s.OrderXML());
            }
        }

    }

    /// <summary>
    /// Used to override the HttpWebResponse for testing
    /// </summary>
    //class BogusHttpWebResponse : HttpWebResponseBase
    //{
    //    public HttpStatusCode _statusCode;
    //    public override HttpStatusCode StatusCode { get { return _statusCode; } }
    //}

    [TestClass]
    public class PostOrder
    {
        /// <summary>
        /// Test when the post is successful
        /// </summary>
        /// <remarks>It would be better if I didn't have to hit the web site to 
        /// post an order each time I ran this test, but it is too complicated 
        /// to make a mock HttpWebResponse that is returned from a mock
        /// WebRequest. Instead I am just marking whatever is returned as successful.</remarks>
        [TestMethod]
        public void SuccessfulPost()
        {
            // Set up the station
            var o = new WindowsFormsApplication1.Order(1, true);
            var s = new Station("7C725161-B511-4C52-B050-44513107FB40",
                1200, 9908, 30, 8864, "NCRCashDrawer.2", 0.065m,
                "", o);
            s.order.items.Add(new OrderItem("Thunder Burger", 595, "No mayo"));
            s.order.items.Add(new OrderItem("Fry Planet", 299, "with cheese"));

            var h = new TMG.Web.WebHelper();
            s.PostOrder(h);

            // Check that everything changed appropriately for a success
            Assert.AreEqual(0, s.order.items.Count);  // Blank order
            Assert.AreEqual(1201ul, s.nextOrderNumber); // Advanced to next order
            Assert.AreEqual("7C725161-B511-4C52-B050-44513107FB40", s.associateID);
            Assert.AreEqual(9908ul, s.storeNumber);
            Assert.AreEqual(30ul, s.terminalNumber);
            Assert.AreEqual(8864ul, s.tableNumber);
            Assert.AreEqual("NCRCashDrawer.2", s.cashDrawer);
            Assert.AreEqual(0.065m, s.taxRate);
            Assert.AreEqual("", s.notes);
            Assert.AreEqual(1u, s.order.seatNumber);
            Assert.AreEqual(true, s.order.isToGoOrder);
        }

        /// <summary>
        /// Test when the post fails
        /// </summary>
        [TestMethod]
        public void FailedPost()
        {
            // Set up the station
            var o = new WindowsFormsApplication1.Order(1, true);
            var s = new Station("7C725161-B511-4C52-B050-44513107FB40",
                1200, 9908, 30, 8864, "NCRCashDrawer.2", 0.065m,
                "", o);
            s.order.items.Add(new OrderItem("Thunder Burger", 595, "No mayo"));
            s.order.items.Add(new OrderItem("Fry Planet", 299, "with cheese"));

            bool exceptionThrown = false;
            try
            {
                var h = new TMG.Web.WebHelper();
                h.Timeout = 1;
                s.PostOrder(h);
            }
            catch (System.IO.IOException)
            {
                exceptionThrown = true;
                // Check that nothing was changed by a failure
                Assert.AreEqual(2, s.order.items.Count);  
                Assert.AreEqual(1200ul, s.nextOrderNumber); 
                Assert.AreEqual("7C725161-B511-4C52-B050-44513107FB40", s.associateID);
                Assert.AreEqual(9908ul, s.storeNumber);
                Assert.AreEqual(30ul, s.terminalNumber);
                Assert.AreEqual(8864ul, s.tableNumber);
                Assert.AreEqual("NCRCashDrawer.2", s.cashDrawer);
                Assert.AreEqual(0.065m, s.taxRate);
                Assert.AreEqual("", s.notes);
                Assert.AreEqual(1u, s.order.seatNumber);
                Assert.AreEqual(true, s.order.isToGoOrder);
            }
            Assert.AreEqual(true, exceptionThrown);
        }

    }


}
