Login/Logout
-----

    If I include a login window (rather than just having current associate set in the station configuration dialog), it needs to be just a simple dialog that pops up when the program is first activated and when the associate logs out.
	
    The login dialog should be modal and say:
    
    Enter your associate ID
    
    AssociateID is a string and should be a masked text box allowing only letters, numbers, spaces, '-', and '.'. (This will ensure it can be output raw for xml)
    
Submit
------

    Needs to display a message box on unsuccessful submit.
    
    Should check that a seat number is present or that to-go is checked. If to-go is checked, the seat number will be set to 1 for all entries. Seat number must be validated during the submission here and an error if there is an invalid seat number when to-go is not checked. 
    
    All strings should be xml escaped to avoid Little Bobby Tables.
    
Edit item
---------
    Brings up a modal dialog

    3 fields: Description, Price, Notes
    Quantity will always be 1
    Seat number and To Go will be set equal for all items from the fields on the main interface
    
    Has OK and Cancel buttons
    
    Field validation error (price is not a non-negative integer number of pennies) will pop up a message box with the error and leave the dialog up.
    
Configure (Gear icon/just text)
---------

    Has 1 field for each of:
    
    * Associate ID (may be absent if I have associates logging in)
    
    * Order Number - 64 bit unsigned integer. Should be validated on input
    
    * Store Number - 64 bit unsigned integer. Should be validated on input
    
    * Terminal Number - 64 bit unsigned integer. Should be validated on input
    
    * Table Number - 64 bit unsigned integer. Should be validated on input
    
    * Cash Drawer - string. See AssociateID for input filtering
    
    * Tax Rate - decimal from 0 to 1
    
    * Table Notes - Multi-line string. 

    These values are stored in the configuration file (except maybe Associate ID)
